﻿namespace ServiceLib.Constant
{
	public static class CreateAssetMenu
	{
		public static class Path
		{
			public const string c_ECS = "ECS/";
			public const string c_System = "Systems/";
			public const string c_StaticData = "Static data/";
			public const string c_Initializers = "Initializers/";
			public const string c_ECS_System = c_ECS + c_System;
			public const string c_ECS_Initializers = c_ECS + c_Initializers;
		}
	}
}