﻿namespace ServiceLib.Constant
{
	public static class AddComponentMenu
	{
		public static class Path
		{
			public const string c_Load = "Load/";
			public const string c_Mechanics = "Mechanics/";
			public const string c_Component = "Component/";
			public const string c_SmoothDamp = "SmoothDamp/";
			public const string c_Installer = "Installer/";
			public const string c_Infrastructure = "Infrastructure/";
		}
	}
}