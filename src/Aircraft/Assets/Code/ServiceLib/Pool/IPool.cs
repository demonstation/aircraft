﻿namespace ServiceLib.Pool
{
	public interface IPool<TItem> where TItem : IPoolItem
	{
		void Push(TItem item);
		TItem Pop();
	}
}