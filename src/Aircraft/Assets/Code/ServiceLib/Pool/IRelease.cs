﻿namespace ServiceLib.Pool
{
	public interface IRelease
	{
		void Release();
	}
}