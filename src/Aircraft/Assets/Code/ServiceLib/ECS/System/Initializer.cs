﻿using Scellecs.Morpeh;
using Zenject;

namespace ServiceLib.ECS.System
{
	public abstract class Initializer : IInitializer
	{
		protected Filter _filter;
		[Inject] public World World { get; set; }
		public abstract void OnAwake();
		public virtual void Dispose() { }
	}
}