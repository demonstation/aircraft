﻿using Scellecs.Morpeh;

namespace ServiceLib.ECS.System.Clean.CleanUp
{
	public abstract class Entities : System.Cleanup
	{
		EntityCleaner _entityCleaner;

		public sealed override void OnAwake()
		{
			var filters = CreateFilters();
			_entityCleaner = new EntityCleaner(World, filters);
		}

		public sealed override void OnUpdate(float deltaTime) => 
			_entityCleaner.OnUpdate();

		protected abstract Filter[] CreateFilters();
	}
}