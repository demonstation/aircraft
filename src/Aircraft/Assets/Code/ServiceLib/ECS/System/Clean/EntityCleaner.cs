﻿using Scellecs.Morpeh;

namespace ServiceLib.ECS.System.Clean
{
	public sealed class EntityCleaner
	{
		readonly Filter[] _filters;
		readonly World _world;

		public EntityCleaner(World world, Filter[] filters)
		{
			_world = world;
			_filters = filters;
		}

		public void OnUpdate()
		{
			foreach (var filter in _filters)
			foreach (var e in filter)
				_world.RemoveEntity(e);
		}
	}
}