﻿using System;

namespace ServiceLib.ECS.System.Clean.Update
{
	public abstract class Components : System.Update
	{
		ComponentCleaner _componentCleaner;

		public override void OnAwake() =>
			_componentCleaner = new ComponentCleaner(World, OneFrameComponentTypes());

		public override void OnUpdate(float deltaTime) =>
			_componentCleaner.OnUpdate();

		protected abstract Type[] OneFrameComponentTypes();
	}
}