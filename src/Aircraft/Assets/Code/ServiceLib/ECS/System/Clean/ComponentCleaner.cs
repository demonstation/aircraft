﻿using System;
using Scellecs.Morpeh;

namespace ServiceLib.ECS.System.Clean
{
	public sealed class ComponentCleaner
	{
		readonly Stash[] _stashes;

		public ComponentCleaner(World world, Type[] typesForClean)
		{
			_stashes = new Stash[typesForClean.Length];

			for (var i = 0; i < typesForClean.Length; i++)
				_stashes[i] = world.GetReflectionStash(typesForClean[i]);
		}

		public void OnUpdate()
		{
			foreach (var stash in _stashes)
				stash.RemoveAll();
		}
	}
}