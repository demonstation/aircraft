﻿using Scellecs.Morpeh;
using Zenject;

namespace ServiceLib.ECS.System
{
	public abstract class Base
	{
		protected Filter _filter;
		[Inject] public World World { get; set; }
		public abstract void OnAwake();
		public abstract void OnUpdate(float deltaTime);

		public virtual void Dispose()
		{ }
	}
}