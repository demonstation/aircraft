﻿using Scellecs.Morpeh;

namespace ServiceLib.ECS.System
{
	public abstract class LateUpdate : Base, ILateSystem { }
}