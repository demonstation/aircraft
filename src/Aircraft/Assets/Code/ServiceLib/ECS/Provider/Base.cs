﻿using Scellecs.Morpeh;
using Scellecs.Morpeh.Providers;
using UnityEngine;

namespace ServiceLib.ECS.Provider
{
	[DisallowMultipleComponent]
	public abstract class Base<T> : MonoProvider<T>
		where T : struct, IComponent { }
}