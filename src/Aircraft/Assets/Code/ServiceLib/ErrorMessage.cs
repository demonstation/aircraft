﻿using UnityEngine;

namespace ServiceLib
{
	public static class ErrorMessage
	{
		public static void ComponentMissingData<T>()
		{
			Debug.LogError($"{nameof(T)} component is missing data.");
		}

		public static void ComponentsMissingData()
		{
			Debug.LogError("Component is missing data.");
		}

		public static void EntityIsNullOrDisposed(string name)
		{
			Debug.LogError($"The \"{name}\" entity is null or disposed.");
		}

		public static void EntityIsNullOrDisposed()
		{
			Debug.LogError($"An entity is null or disposed.");
		}

		public static void EntityHasNoComponent<T>()
		{
			Debug.LogError($"The entity has not component: {typeof(T)}");
		}

		public static void EcsBridgeMissing()
		{
			Debug.LogError("ECS bridge is missing.");
		}

		public static void GameObjectHasNoComponent<T>(GameObject go)
		{
			Debug.LogError(
				$"The \"{go.name}\" game object has not \"{typeof(T)}\" component.");
		}

		public static void HasNoEntityForGameObject(string name)
		{
			if (name == null)
			{
				HasNoEntityForGameObject();
				return;
			}

			Debug.LogError($"Has no an entity for \"{name}\" game object.");
		}

		public static void HasNoEntityForGameObject()
		{
			Debug.LogError("Has no an entity for a game object.");
		}

		public static void NotMatchEntityIds()
		{
			Debug.LogError("The entity IDs do not match.");
		}
	}
}