﻿using System.Collections.Generic;

namespace ServiceLib.Extension
{
	public static class ServiceLocatorExtension
	{
		public static void RegisterRange<TType>(this ServiceLocator<TType> locator,
			IEnumerable<TType> range)
		{
			foreach (var item in range) 
				locator.Register(item);
		}
	}
}