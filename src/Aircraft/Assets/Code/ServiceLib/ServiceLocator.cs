﻿using System;
using System.Collections.Generic;

namespace ServiceLib
{
	public class ServiceLocator<TType>
	{
		readonly Dictionary<Type, TType> _dictionary = new();

		public TType this[Type t] => _dictionary[t];

		public ServiceLocator<TType> Register(TType instance)
		{
			_dictionary[instance.GetType()] = instance;
			return this;
		}

		public ServiceLocator<TType> RegisterAs<T>(TType instance)
			where T : TType
		{
			_dictionary[typeof(T)] = instance;
			return this;
		}

		public ServiceLocator<TType> Unregister<T>() where T : TType
		{
			_dictionary.Remove(typeof(T));
			return this;
		}

		public TType Get<T>() where T : TType
		{
			return _dictionary[typeof(T)];
		}
	}
}