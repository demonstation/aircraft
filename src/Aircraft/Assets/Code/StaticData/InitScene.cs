﻿using UnityEngine;
using Menu = ServiceLib.Constant.CreateAssetMenu;

namespace StaticData
{
	[CreateAssetMenu(menuName = Menu.Path.c_StaticData + nameof(InitScene))]
	public class InitScene : ScriptableObject
	{
		public string Name;
	}
}