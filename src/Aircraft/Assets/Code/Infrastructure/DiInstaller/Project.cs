﻿using Infrastructure.ECS;
using Infrastructure.GameStatus;
using Infrastructure.Services;
using Infrastructure.Services.Input;
using Scellecs.Morpeh;
using Service;
using Service.Input;
using StaticData;
using UnityEngine;
using Zenject;
using Menu = ServiceLib.Constant.AddComponentMenu;

namespace Infrastructure.DiInstaller
{
	[AddComponentMenu(Menu.Path.c_Infrastructure + Menu.Path.c_Installer +
		nameof(Project))]
	public sealed class Project : MonoInstaller, IInitializable
	{
		[Header("Static data")]
		[SerializeField] InitScene _initScene;
		[SerializeField] GameSettings _gameSettings;
		[SerializeField] DirectLinkProvider _assetProvider;

		[SerializeField] Mechanics.Collectible.StaticData.Coin _coinData;

		public override void InstallBindings()
		{
			// For project.
			Container.Bind<IInitializable>().FromInstance(this).AsSingle();

			Container.Bind<IGameFactory>().To<GameFactory>().AsSingle();
			Container.Bind<ISceneLoader>().To<JustLoader>().AsSingle();
			Container.Bind<IAssetProvider>().FromInstance(_assetProvider).AsSingle();
			Container.BindInstance(_initScene).AsSingle();
			Container.Bind<IInput>().To<InputService>().AsSingle();
			Container.Bind<IGameSettings>().FromInstance(_gameSettings).AsSingle();

			Container.Bind<Game>().AsSingle();
			Container.Bind<IGameStateMachine>().To<GameStateMachine>()
				.AsSingle().NonLazy();
			Container.Bind<IState>().To<LoadGame>().AsSingle();
			Container.Bind<IState>().To<LoadScene>().AsSingle();
			Container.Bind<IState>().To<GameLoop>().AsSingle();

			Container.Bind<World>().FromInstance(World.Default).AsSingle();

			// For sub containers.
			Container.Bind<SystemGroupBuilder>().AsTransient()
				.MoveIntoDirectSubContainers();
			Container.Bind<EcsWorldBuilder>().AsCached()
				.MoveIntoDirectSubContainers();

			// Bind static data.
			Container.BindInstance(_coinData).AsSingle();
		}

		public void Initialize()
		{
			Container.Resolve<Game>().Start();
		}
	}
}