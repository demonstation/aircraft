using Infrastructure.ECS;
using Infrastructure.GameStatus;
using Scellecs.Morpeh;
using Zenject;

namespace Infrastructure.DiInstaller
{
	public sealed class DefaultScene : MonoInstaller, IInitializable
	{
		[Inject] World _world;
		[Inject] EcsWorldBuilder _ecsWorldBuilder;
		[Inject] IGameStateMachine _gameStateMachine;

		public override void InstallBindings()
		{
			Container.Bind<ISceneInit>().FromComponentInHierarchy().AsCached();
			BindCasters();
			BindInitializable();
			BindInstaller();
		}

		public void Initialize()
		{
			BindSystems();
			Container.Resolve<ECS.Installer>().InitWorld();
			Container.Resolve<ISceneInit>().Init();
			_gameStateMachine.Enter<GameLoop>();
		}

		void BindSystems()
		{
			_ecsWorldBuilder.CreateGroups();
			_ecsWorldBuilder.BindSystems();
		}

		void BindInstaller()
		{
			Container.BindInterfacesAndSelfTo<ECS.Installer>().AsCached();
		}

		void BindInitializable()
		{
			Container.Bind<IInitializable>().FromInstance(this).AsCached();
		}

		void BindCasters()
		{
			Container.Bind<Mechanics.Health.RequestCast>().AsCached();
			Container.Bind<Mechanics.Health.EventCast>().AsCached();
			Container.Bind<Mechanics.Collectible.EventCast>().AsCached();
		}
	}
}