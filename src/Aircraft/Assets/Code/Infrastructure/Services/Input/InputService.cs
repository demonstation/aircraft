﻿using Service;
using Service.Input;
using ServiceLib;

namespace Infrastructure.Services.Input
{
	public sealed class InputService : IInput
	{
		readonly ServiceLocator<IInputMap> _maps = new ServiceLocator<IInputMap>();

		public InputService()
		{
			_maps
				.RegisterAs<IAircraft>(new Aircraft())
				;
		}

		public TMap Map<TMap>() where TMap : class, IInputMap =>
			_maps.Get<TMap>() as TMap;
	}
}