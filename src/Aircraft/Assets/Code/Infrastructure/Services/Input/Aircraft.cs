using System;
using Service.Input;
using UnityEngine.InputSystem;

namespace Infrastructure.Services.Input
{
	public class Aircraft : IAircraft, IDisposable
	{
		readonly ActionsMap _map = new();

		public event Action ShotStarted;
		public event Action ShotCanceled;
		public event Action ShotPerformed;

		public float AircraftManeuvering { get; private set; }

		public Aircraft()
		{
			_map.Aircraft.Shot.started += OnAircraftShotStarted;
			_map.Aircraft.Shot.canceled += OnAircraftShotCanceled;
			_map.Aircraft.Shot.performed += OnShotPerformed;
			_map.Aircraft.Maneuvering.performed += OnAircraftManeuveringPerformed;
		}

		public void Enable()
		{
			_map.Enable();
		}

		public void Disable()
		{
			_map.Disable();
		}

		public void Dispose()
		{
			Disable();
			_map.Aircraft.Shot.started -= OnAircraftShotStarted;
			_map.Aircraft.Shot.canceled -= OnAircraftShotCanceled;
			_map.Aircraft.Shot.performed -= OnShotPerformed;
			_map.Aircraft.Maneuvering.performed -= OnAircraftManeuveringPerformed;
		}

		void OnAircraftManeuveringPerformed(InputAction.CallbackContext context)
		{
			AircraftManeuvering = context.ReadValue<float>();
		}

		void OnAircraftShotStarted(InputAction.CallbackContext context)
		{
			ShotStarted?.Invoke();
		}

		void OnAircraftShotCanceled(InputAction.CallbackContext context)
		{
			ShotCanceled?.Invoke();
		}

		void OnShotPerformed(InputAction.CallbackContext context)
		{
			ShotPerformed?.Invoke();
		}
	}
}