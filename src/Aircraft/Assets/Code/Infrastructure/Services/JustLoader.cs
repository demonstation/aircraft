﻿using Cysharp.Threading.Tasks;
using Service;
using UnityEngine.SceneManagement;

namespace Infrastructure.Services
{
	public class JustLoader : ISceneLoader
	{
		public async UniTask LoadAsync(string sceneName)
		{
			await SceneManager.LoadSceneAsync(sceneName).ToUniTask();
		}
	}
}