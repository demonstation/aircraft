﻿using Service;
using UnityEngine;

namespace Infrastructure.Services
{
	[CreateAssetMenu()]
	public sealed class GameSettings : ScriptableObject, IGameSettings
	{
		[field: SerializeField] public Vector3 ExplosionEffectDirection { get; private set; }
	}
}