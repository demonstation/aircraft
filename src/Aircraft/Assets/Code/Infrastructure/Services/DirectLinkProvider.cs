﻿using Service;
using UnityEngine;
using Menu = ServiceLib.Constant.CreateAssetMenu;

namespace Infrastructure.Services
{
	[CreateAssetMenu(menuName =
		Menu.Path.c_StaticData + nameof(DirectLinkProvider))]
	public class DirectLinkProvider : ScriptableObject, IAssetProvider
	{
		[SerializeField] GameObject _hudRoot;
		[SerializeField] GameObject _coinCounterHUD;
		[SerializeField] GameObject _healthBarHud;
		[SerializeField] GameObject _bullet;
		[SerializeField] GameObject _aircraft;
		[SerializeField] GameObject _playerEntity;
		[SerializeField] GameObject _bombExplosion;
		[SerializeField] GameObject _mainCamera;
		[SerializeField] GameObject _coin;

		public GameObject HUDRoot() => _hudRoot;
		public GameObject CoinCounterHUD() => _coinCounterHUD;
		public GameObject HealthBarHud() => _healthBarHud;
		public GameObject Bullet() => _bullet;
		public GameObject Aircraft() => _aircraft;
		public GameObject PlayerEntity() => _playerEntity;
		public GameObject BombExplosion() => _bombExplosion;

		public
			GameObject MainCamera() => _mainCamera;

		public GameObject Coin() => _coin;
	}
}