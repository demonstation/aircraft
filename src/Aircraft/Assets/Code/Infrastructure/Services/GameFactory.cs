﻿using Extension.UnityEngine;
using Scellecs.Morpeh;
using Service;
using ServiceLib;
using UnityEngine;
using Zenject;

namespace Infrastructure.Services
{
	public sealed class GameFactory : IGameFactory
	{
		[Inject] DiContainer _diContainer;
		[Inject] IAssetProvider _assetProvider;

		public GameObject CreateRootHUD()
		{
			return Object.Instantiate(_assetProvider.HUDRoot());
		}

		public GameObject CreateHealthBar(Transform parent)
		{
			return Object.Instantiate(_assetProvider.HealthBarHud());
		}

		public GameObject CreateCoinCounterHud(Transform parent)
		{
			return Object.Instantiate(_assetProvider.CoinCounterHUD(), parent);
		}

		public GameObject CreateAircraft(Vector3 pos, Quaternion rot)
		{
			return Object.Instantiate(_assetProvider.Aircraft(), pos, rot);
		}

		public GameObject BulletProjectile(Vector3 pos, Quaternion rot)
		{
			return Object.Instantiate(_assetProvider.Bullet(), pos, rot);
		}

		public Entity CreatePlayerEntity()
		{
			var prefab = _assetProvider.PlayerEntity();
			var obj = Object.Instantiate(prefab);
			if (obj.TryGetEntity(out var entity) == false)
			{
				ErrorMessage.HasNoEntityForGameObject(obj.name);
				return default;
			}

			return entity;
		}

		public GameObject CreateBombExplosionEffect(Vector3 explosionPos,
			Quaternion rot)
		{
			var prefab = _assetProvider.BombExplosion();
			var gameObject = Object.Instantiate(prefab, explosionPos, rot);
			return gameObject;
		}

		public GameObject CreateCamera()
		{
			var prefab = _assetProvider.MainCamera();
			return Object.Instantiate(prefab);
		}

		public GameObject CreateCoin(Vector3 position, Quaternion rotation,
			Transform parent)
		{
			var prefab = _assetProvider.Coin();
			return _diContainer.InstantiatePrefab(prefab, position, rotation, parent);
		}

		public void Release(GameObject gameObject)
		{
			Object.Destroy(gameObject);
		}
	}
}