﻿namespace Infrastructure.GameStatus
{
	public interface IState
	{
		void Enter();
		void Exit();
	}
}