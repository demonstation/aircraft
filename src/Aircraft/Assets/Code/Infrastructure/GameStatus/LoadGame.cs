﻿using StaticData;
using Zenject;

namespace Infrastructure.GameStatus
{
	public sealed class LoadGame : State, IState
	{
		[Inject] InitScene _initScene;

		public LoadGame(IGameStateMachine gameStateMachine) : base(gameStateMachine)
		{ }

		public void Enter()
		{ }

		public void Exit()
		{ }
	}
}