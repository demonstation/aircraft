﻿using Service;
using Service.Input;
using Zenject;

namespace Infrastructure.GameStatus
{
	public sealed class GameLoop : State, IState
	{
		[Inject] IInput _input;

		public GameLoop(IGameStateMachine gameStateMachine) :
			base(gameStateMachine) { }

		public void Enter()
		{
			_input.Map<IAircraft>().Enable();
		}

		public void Exit()
		{
			_input.Map<IAircraft>().Disable();
		}
	}
}