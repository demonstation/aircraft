﻿using Service;
using Zenject;

namespace Infrastructure.GameStatus
{
	public sealed class LoadScene : State, IState
	{
		[Inject] ISceneLoader _sceneLoader;

		public string LoadingScene { set; get; }

		public LoadScene(IGameStateMachine gameStateMachine) :
			base(gameStateMachine)
		{ }

		public async void Enter()
		{
			await _sceneLoader.LoadAsync(LoadingScene);
			_gameStateMachine.Enter<GameLoop>();
		}

		public void Exit()
		{ }
	}
}