﻿using Extension;
using Extension.UnityEngine;
using Mechanics.CameraManeuvering.Component;
using Mechanics.Collectible.Coin.Component;
using Mechanics.HUD.Coin.Component;
using Mechanics.HUD.Health.Component;
using Mechanics.Spawn.Component;
using Scellecs.Morpeh;
using Service;
using ServiceLib;
using UnityEngine;
using Zenject;

namespace Infrastructure
{
	public sealed class GameInit : MonoBehaviour, ISceneInit
	{
		[SerializeField] AircraftProvider _aircraftSpawnPoint;

		[Inject] World _world;
		[Inject] IGameFactory _factory;

		public void Init()
		{
			var aircraft = CreateAircraft();
			var playerEntity = _factory.CreatePlayerEntity();
			if (TryInitAircraft(aircraft.entity, playerEntity) == false)
				return;

			TryInitCamera(aircraft.obj, aircraft.entity);

			CreateHud(playerEntity, aircraft.entity);
		}

		bool TryInitCamera(GameObject aircraftObj, Entity aircraftEntity)
		{
			var mainCamera = _factory.CreateCamera();
			if (!CanProcessCamera(mainCamera, aircraftEntity, out var cameraEntity))
				return false;

			InitCameraFollow(aircraftEntity, cameraEntity);
			InitCameraRotationTracker(aircraftObj, cameraEntity);

			return true;
		}

		void InitCameraFollow(Entity aircraftEntity, Entity cameraEntity)
		{
			ref var cameraFollow = ref CameraFollow(cameraEntity);

			ref var followingTarget =
				ref aircraftEntity.GetComponent<CameraFollowingTarget>();

			cameraFollow.Target = followingTarget.Value;
		}

		void InitCameraRotationTracker(GameObject aircraftObj, Entity cameraEntity)
		{
			ref var rotationTracker =
				ref cameraEntity.GetComponent<RotationTracker>();
			rotationTracker.Target = aircraftObj.transform;
		}

		bool CanProcessCamera(GameObject mainCamera, Entity aircraftEntity,
			out Entity cameraEntity)
		{
			if (mainCamera.TryGetEntity(out cameraEntity) == false)
			{
				ErrorMessage.HasNoEntityForGameObject(mainCamera.name);
				return false;
			}

			if (cameraEntity.Has<CameraInterpolationFollow>() == false)
			{
				ErrorMessage.EntityHasNoComponent<CameraInterpolationFollow>();
				return false;
			}

			if (cameraEntity.Has<RotationTracker>() == false)
			{
				ErrorMessage.EntityHasNoComponent<RotationTracker>();
				return false;
			}

			if (aircraftEntity.Has<CameraFollowingTarget>() == false)
			{
				ErrorMessage.EntityHasNoComponent<CameraFollowingTarget>();
				return false;
			}

			return true;
		}

		ref CameraInterpolationFollow CameraFollow(Entity cameraEntity)
		{
			return ref cameraEntity
				.GetComponent<Mechanics.CameraManeuvering.Component.
					CameraInterpolationFollow>();
		}

		(GameObject obj, Entity entity) CreateAircraft()
		{
			return Mechanics.Spawn.System.Aircraft.CreateAircraft(
				_aircraftSpawnPoint.transform, _factory);
		}

		bool TryInitAircraft(Entity aircraftEntity, Entity coinCounterEntity)
		{
			if (HasCoinCollectorEntity(aircraftEntity) == false)
				return false;

			if (coinCounterEntity.Has<CoinCounter>() == false)
			{
				ErrorMessage.EntityHasNoComponent<CoinCounter>();
				return false;
			}

			ref var coinCollector = ref aircraftEntity.GetComponent<CoinCollector>();
			coinCollector.CounterEntity = coinCounterEntity;

			return true;
		}

		void CreateHud(Entity playerEntity, Entity aircraftHealthEntity)
		{
			var rootHUD = _factory.CreateRootHUD();
			CreateCoinCounter(rootHUD.transform, playerEntity);
			CreateHealthBar(rootHUD.transform, aircraftHealthEntity);
		}

		void CreateCoinCounter(Transform parent, Entity coinCounterEntity)
		{
			var coinCounterHud = _factory.CreateCoinCounterHud(parent);

			if (TryGetScreenEntity(coinCounterHud, out var coinScreenEntity) == false)
				return;

			coinCounterEntity.AddComponent((ref CoinTracker c) =>
			{
				c.CoinScreenEntity = coinScreenEntity;
			});

			ref var coinScreen = ref coinScreenEntity.GetComponent<CoinScreen>();
			coinScreen.TrackerEntity = coinCounterEntity;
		}

		void CreateHealthBar(Transform parent, Entity healthEntity)
		{
			var healthBar = _factory.CreateHealthBar(parent);

			if (TryGetHealthBarEntity(healthBar, out var screenEntity) == false)
				return;

			healthEntity.AddComponent((ref HealthTracker c) =>
			{
				c.HealthScreenEntity = screenEntity;
			});

			ref var healthScreen =
				ref screenEntity.GetComponent<HealthScreen>();
			healthScreen.TrackerEntity = healthEntity;

			CastUpdateHealthHUDRequest(healthEntity, screenEntity);
		}

		void CastUpdateHealthHUDRequest(Entity healthEntity, Entity screenEntity)
		{
			_world.CreateEntity().AddComponent((ref UpdateScreenRequest c) =>
			{
				c.HealthEntity = healthEntity;
				c.ScreenEntity = screenEntity;
			});
		}

		bool TryGetHealthBarEntity(GameObject healthBar, out Entity entity)
		{
			if (healthBar.TryGetEntity(out entity) == false)
			{
				ErrorMessage.HasNoEntityForGameObject(healthBar.name);
				return false;
			}

			if (entity.Has<HealthScreen>() == false)
			{
				ErrorMessage.EntityHasNoComponent<HealthScreen>();
				return false;
			}

			return true;
		}

		bool TryGetScreenEntity(GameObject coinCounterHud, out Entity screenEntity)
		{
			if (coinCounterHud.TryGetEntity(out screenEntity) == false)
			{
				ErrorMessage.HasNoEntityForGameObject(coinCounterHud.name);
				return false;
			}

			if (screenEntity.Has<CoinScreen>() == false)
			{
				ErrorMessage.EntityHasNoComponent<CoinScreen>();
				return false;
			}

			return true;
		}

		bool HasCoinCollectorEntity(Entity aircraftEntity)
		{
			if (aircraftEntity.Has<CoinCollector>() == false)
			{
				ErrorMessage.EntityHasNoComponent<CoinCollector>();
				return false;
			}

			return true;
		}
	}
}