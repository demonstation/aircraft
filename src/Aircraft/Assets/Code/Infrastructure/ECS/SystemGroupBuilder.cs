﻿using System;
using System.Collections.Generic;
using Scellecs.Morpeh;
using Zenject;

namespace Infrastructure.ECS
{
	public sealed class SystemGroupBuilder : IDisposable
	{
		SystemsGroup _systemGroup;

		[Inject] readonly World _world;
		[Inject] readonly DiContainer _diContainer;

		readonly List<Action> _bindings = new();
		readonly List<Action> _groupings = new();

		public SystemsGroup SystemGroup => _systemGroup;

		public SystemGroupBuilder CreateGroup()
		{
			_systemGroup = _world.CreateSystemsGroup();
			return this;
		}

		public SystemGroupBuilder AddInitializer<TSystem>()
			where TSystem : class, IInitializer
		{
			_bindings.Add(() => _diContainer.Bind<TSystem>().AsSingle());
			_groupings.Add(() =>
				_systemGroup.AddInitializer(_diContainer.Resolve<TSystem>()));
			return this;
		}

		public SystemGroupBuilder AddSystem<TSystem>()
			where TSystem : class, ISystem
		{
			_bindings.Add(() => _diContainer.Bind<TSystem>().AsSingle());
			_groupings.Add(() =>
				_systemGroup.AddSystem(_diContainer.Resolve<TSystem>()));
			return this;
		}

		public void BindSystemsToDiContainer()
		{
			foreach (var binding in _bindings)
				binding.Invoke();
		}

		public void AddSystemsToSystemGroups()
		{
			foreach (var grouping in _groupings)
				grouping.Invoke();
		}

		public void Dispose()
		{
			_bindings.Clear();
			_groupings.Clear();
		}
	}
}