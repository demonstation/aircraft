﻿using System;
using System.Collections.Generic;
using Extension;
using Scellecs.Morpeh;
using Zenject;

namespace Infrastructure.ECS
{
	public sealed class EcsWorldBuilder : IDisposable
	{
		[Inject] readonly World _world;
		[Inject] readonly SystemGroupBuilder _groupBuilder;

		readonly List<SystemsGroup> _groups = new();

		public void CreateGroups()
		{
			_groups
				.AddAnd(SpawnGroup())
				.AddAnd(EcsBridgeGroup())
				// Input.
				.AddAnd(AircraftInputProcessingGroup())
				// Mechanics.
				.AddAnd(ColliderEventGroup())
				.AddAnd(CollectibleGroup())
				.AddAnd(CoinGroup())
				.AddAnd(CoinHUDGroup())
				.AddAnd(ExplosionGroup())
				.AddAnd(ManeuveringGroup())
				.AddAnd(ShootingGroup())
				.AddAnd(HealthGroup())
				.AddAnd(HealthHUDGroup())
				//
				.AddAnd(CameraManeuveringGroup())
				.AddAnd(LastGroup())
				;
		}

		public void BindSystems() =>
			_groupBuilder.BindSystemsToDiContainer();

		public IReadOnlyList<SystemsGroup> AddSystemsToSystemGroup()
		{
			_groupBuilder.AddSystemsToSystemGroups();
			return _groups;
		}

		public void ClearWorld()
		{
			foreach (var group in _groups)
				_world.RemoveSystemsGroup(group);
		}

		public void Dispose()
		{
			_groups.Clear();
			_groupBuilder?.Dispose();
		}

		SystemsGroup SpawnGroup()
		{
			return _groupBuilder.CreateGroup()
				// Update.
				.AddSystem<Mechanics.Spawn.System.EventEntities>()
				.AddSystem<Mechanics.Spawn.System.Aircraft>()
				.AddSystem<Mechanics.Spawn.System.Coins>()
				.AddSystem<Mechanics.Spawn.System.RequestComponentsClean>()
				.SystemGroup;
		}

		SystemsGroup EcsBridgeGroup()
		{
			return _groupBuilder.CreateGroup()
				// Update.
				.AddSystem<Mechanics.ECSBridge.System.EcsBridgeInit>()
				.SystemGroup;
		}

		SystemsGroup AircraftInputProcessingGroup()
		{
			return _groupBuilder.CreateGroup()
				// Initialize.
				.AddInitializer<Mechanics.AircraftInputProcessing.System.
					SemiAutomaticShootingProcessing>()
				// Update.
				.AddSystem<Mechanics.AircraftInputProcessing.System.
					AutomaticShootingProcessing>()
				.AddSystem<Mechanics.AircraftInputProcessing.System.TorqueProcessing>()
				.SystemGroup;
		}

		SystemsGroup ColliderEventGroup()
		{
			return _groupBuilder.CreateGroup()
				// Cleanup.
				.AddSystem<Mechanics.CollisionEvent.System.EventEntitiesClean>()
				.SystemGroup;
		}

		SystemsGroup CollectibleGroup()
		{
			return _groupBuilder.CreateGroup()
				// Update.
				.AddSystem<Mechanics.Collectible.System.DestroyCollectible>()
				.AddSystem<Mechanics.Collectible.System.EventEntitiesClean>()
				.AddSystem<Mechanics.Collectible.System.CollectProcess>()
				.SystemGroup;
		}

		SystemsGroup CoinGroup()
		{
			return _groupBuilder.CreateGroup()
				// Update
				.AddSystem<Mechanics.Collectible.Coin.System.EventEntities>()
				.AddSystem<Mechanics.Collectible.Coin.System.EventComponentsClean>()
				.AddSystem<Mechanics.Collectible.Coin.System.CoinCollect>()
				.AddSystem<Mechanics.Collectible.Coin.System.ProcessCoinCounter>()
				.SystemGroup;
		}

		SystemsGroup CoinHUDGroup()
		{
			return _groupBuilder.CreateGroup()
				// LateUpdate.
				.AddSystem<Mechanics.HUD.Coin.System.CoinScreen>()
				.SystemGroup;
		}

		SystemsGroup ExplosionGroup()
		{
			return _groupBuilder.CreateGroup()
				// Update.
				.AddSystem<Mechanics.Explosion.System.Trigger>()
				.AddSystem<Mechanics.Explosion.System.Process>()
				.AddSystem<Mechanics.Explosion.System.RequestComponent>()
				.SystemGroup;
		}

		SystemsGroup ManeuveringGroup()
		{
			return _groupBuilder.CreateGroup()
				// FixedUpdate.
				.AddSystem<Mechanics.Maneuvering.System.AddForceMovement>()
				.AddSystem<Mechanics.Maneuvering.System.AddTorqueRotation>()
				.SystemGroup;
		}

		SystemsGroup ShootingGroup()
		{
			return _groupBuilder.CreateGroup()
				// Update.
				.AddSystem<Mechanics.Shooting.System.EventComponents>()
				.AddSystem<Mechanics.Shooting.System.AutomaticShooting>()
				.AddSystem<Mechanics.Shooting.System.SemiAutomaticShooting>()
				.AddSystem<Mechanics.Shooting.System.ShootMaker>()
				.AddSystem<Mechanics.Shooting.System.ShootSound>()
				.AddSystem<Mechanics.Shooting.System.RequestComponent>()
				.SystemGroup;
		}

		SystemsGroup HealthGroup()
		{
			return _groupBuilder.CreateGroup()
				// Update.
				.AddSystem<Mechanics.Health.System.EventEntities>()
				.AddSystem<Mechanics.Health.System.ApplyDamage>()
				.AddSystem<Mechanics.Health.System.RequestEntity>()
				.SystemGroup;
		}

		SystemsGroup HealthHUDGroup()
		{
			return _groupBuilder.CreateGroup()
				// Update.
				.AddSystem<Mechanics.HUD.Health.System.UpdateScreenRequest>()
				// LateUpdate.
				.AddSystem<Mechanics.HUD.Health.System.HealthScreen>()
				// CleanUp.
				.AddSystem<Mechanics.HUD.Health.System.RequestEntityClean>()
				.SystemGroup;
		}

		SystemsGroup CameraManeuveringGroup()
		{
			return _groupBuilder.CreateGroup()
				// LateUpdate.
				.AddSystem<Mechanics.CameraManeuvering.System.Following>()
				.AddSystem<Mechanics.CameraManeuvering.System.RotationTracking>()
				.SystemGroup;
		}

		SystemsGroup LastGroup()
		{
			return _groupBuilder.CreateGroup()
				// Update.
				.AddSystem<Mechanics.ECSShare.System.DestroyTimer>()
				.SystemGroup;
		}
	}
}