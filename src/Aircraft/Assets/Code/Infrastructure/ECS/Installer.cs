﻿using System;
using System.Collections.Generic;
using Scellecs.Morpeh;
using Zenject;

namespace Infrastructure.ECS
{
	public class Installer : IDisposable
	{
		[Inject] readonly World _world;
		[Inject] readonly EcsWorldBuilder _ecsWorldBuilder;

		public void InitWorld()
		{
			_world.UpdateByUnity = true;
			var systemGroups = _ecsWorldBuilder.AddSystemsToSystemGroup();
			AddSystemGroupsToWorld(systemGroups);
		}

		void AddSystemGroupsToWorld(IReadOnlyList<SystemsGroup> systemGroups)
		{
			for (int i = 0; i < systemGroups.Count; i++)
				_world.AddSystemsGroup(i, systemGroups[i]);
		}

		public void Dispose()
		{
			_ecsWorldBuilder.ClearWorld();
		}
	}
}