﻿using Infrastructure.GameStatus;
using Zenject;

namespace Infrastructure
{
	public sealed class Game
	{
		[Inject] IGameStateMachine _gameStateMachine;

		public void Start()
		{
			_gameStateMachine.Enter<LoadGame>();
		}
	}
}