﻿using Scellecs.Morpeh;
using ServiceLib.ECS.Provider;
using ServiceLib.Pool;

namespace Infrastructure.Demo.Component
{
	public struct PoolObject : IComponent
	{
		public IRelease Release;
	}

	public sealed class PoolObjectProvider : Base<PoolObject>
	{ }
}