﻿using ServiceLib.Pool;
using UnityEngine;

namespace Infrastructure.Demo
{
	public abstract class BasePoolItem : IPoolItem
	{
		public GameObject GameObject { get; }
		
		protected BasePoolItem(GameObject gameObject)
		{
			GameObject = gameObject;
		}

		public abstract void Release();
	}
}