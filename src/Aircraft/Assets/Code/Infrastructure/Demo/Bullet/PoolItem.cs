﻿using UnityEngine;

namespace Infrastructure.Demo.Bullet
{
	public sealed class PoolItem : BasePoolItem
	{
		readonly Realization _pool;

		public Transform Transform { get; }
		public Rigidbody Rigidbody { get; }

		public PoolItem(Realization pool, GameObject gameObject,
			Transform transform,
			Rigidbody rigidbody) : base(gameObject)
		{
			_pool = pool;
			Transform = transform;
			Rigidbody = rigidbody;
		}

		public override void Release()
		{
			_pool.Push(this);
		}
	}
}