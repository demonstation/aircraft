﻿using Infrastructure.Demo.Component;
using Service;
using ServiceLib;
using ServiceLib.Pool;
using UnityEngine;
using UnityEngine.Pool;
using Zenject;

namespace Infrastructure.Demo.Bullet
{
	public sealed class Realization : IPool<PoolItem>
	{
		readonly GameObject _prefab;
		readonly LinkedPool<PoolItem> _pool;

		public Realization(GameObject prefab)
		{
			_prefab = prefab;
			_pool = new LinkedPool<PoolItem>(CreateFunc, ActionOnGet,
				ActionOnRelease, ActionOnDestroy);
		}

		public void Push(PoolItem item)
		{
			_pool.Release(item);
		}

		public PoolItem Pop()
		{
			return _pool.Get();
		}

		PoolItem CreateFunc()
		{
			var instance = Object.Instantiate(_prefab);
			var item = CreatePoolItem(instance);
			InitPoolObjectComponent(item);
			return item;
		}

		void ActionOnGet(PoolItem item)
		{
			item.GameObject.SetActive(true);
		}

		void ActionOnRelease(PoolItem item)
		{
			item.GameObject.SetActive(false);
		}

		void ActionOnDestroy(PoolItem item)
		{
			Object.Destroy(item.GameObject);
		}

		PoolItem CreatePoolItem(GameObject instance)
		{
			if (instance.TryGetComponent<Rigidbody>(out var rigidbody) == false)
				ErrorMessage.GameObjectHasNoComponent<Rigidbody>(instance);

			return new PoolItem(this, instance, instance.transform, rigidbody);
		}

		void InitPoolObjectComponent(PoolItem item)
		{
			if (item.GameObject.TryGetComponent<PoolObjectProvider>(
				    out var poolObjectProvider) == false)
				ErrorMessage.GameObjectHasNoComponent<PoolObjectProvider>(item.GameObject);

			ref var poolObject = ref poolObjectProvider.GetData();
			poolObject.Release = item;
		}
	}
}