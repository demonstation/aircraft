﻿namespace Service
{
	public interface IInput
	{
		TMap Map<TMap>() where TMap : class, IInputMap;
	}

	public interface IInputMap { }
}