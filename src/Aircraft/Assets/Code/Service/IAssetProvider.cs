﻿using UnityEngine;

namespace Service
{
	public interface IAssetProvider : IService
	{
		GameObject HUDRoot();
		GameObject CoinCounterHUD();
		GameObject HealthBarHud();
		GameObject Bullet();
		GameObject Aircraft();
		GameObject PlayerEntity();
		GameObject BombExplosion();
		GameObject MainCamera();
		GameObject Coin();
	}
}