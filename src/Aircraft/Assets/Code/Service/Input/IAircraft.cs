﻿using System;

namespace Service.Input
{
	public interface IAircraft : IInputMap
	{
		event Action ShotPerformed;
		float AircraftManeuvering { get; }
		void Enable();
		void Disable();
		event Action ShotStarted;
		event Action ShotCanceled;
	}
}