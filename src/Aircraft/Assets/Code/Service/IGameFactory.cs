﻿using Scellecs.Morpeh;
using UnityEngine;

namespace Service
{
	public interface IGameFactory : IService
	{
		GameObject CreateRootHUD();
		GameObject CreateHealthBar(Transform parent);
		GameObject CreateCoinCounterHud(Transform parent);
		GameObject CreateAircraft(Vector3 pos, Quaternion rot);
		GameObject BulletProjectile(Vector3 pos, Quaternion rot);
		Entity CreatePlayerEntity();
		GameObject CreateBombExplosionEffect(Vector3 explosionPos, Quaternion rot);
		GameObject CreateCamera();
		GameObject CreateCoin(Vector3 position, Quaternion rotation,
			Transform parent);
		void Release(GameObject gameObject);
	}
}