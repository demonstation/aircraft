﻿using Cysharp.Threading.Tasks;

namespace Service
{
	public interface ISceneLoader : IService
	{
		UniTask LoadAsync(string sceneName);
	}
}