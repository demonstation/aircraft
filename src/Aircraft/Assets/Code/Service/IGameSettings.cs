﻿using UnityEngine;

namespace Service
{
	public interface IGameSettings : IService
	{
		Vector3 ExplosionEffectDirection { get; }
	}
}