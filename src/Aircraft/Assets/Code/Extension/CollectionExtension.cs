﻿using System.Collections.Generic;

namespace Extension
{
	public static class CollectionExtension
	{
		public static ICollection<T> AddAnd<T>(this ICollection<T> collection,
			T item)
		{
			collection.Add(item);
			return collection;
		}
	}
}