﻿using Scellecs.Morpeh.Collections;
using Scellecs.Morpeh.Providers;

namespace Extension.Util
{
	public static class Entity
	{
		public static bool TeyGetEntity(int instanceID, out Scellecs.Morpeh.Entity entity)
		{
			if (EntityProvider.map.TryGetValue(instanceID, out var item) == false)
			{
				entity = null;
				return false;
			}

			entity = item.entity;
			return true;
		}
	}
}