﻿using Scellecs.Morpeh;

namespace Extension.Util
{
	public static class World
	{
		public static void RemoveAllComponents<TComponent>()
			where TComponent : struct, IComponent
		{
			Scellecs.Morpeh.World.Default.GetStash<TComponent>().RemoveAll();
		}
	}
}