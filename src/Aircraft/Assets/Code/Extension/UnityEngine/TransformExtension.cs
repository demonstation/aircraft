﻿using UnityEngine;

namespace Extension.UnityEngine
{
	public static class TransformExtension
	{
		public static Vector3 Back(this Transform transform) =>
			-transform.forward;

		public static Vector3 Left(this Transform transform) =>
			-transform.right;

		public static Vector3 VectorTo(this Transform transform,
			Transform target) =>
			target.position - transform.position;
	}
}