﻿using Scellecs.Morpeh;
using UnityEngine;

namespace Extension.UnityEngine
{
	public static class ObjectExtension
	{
		public static bool TryGetEntity(this Object go, out Entity entity)
		{
			var instanceID = go.GetInstanceID();
			if (Util.Entity.TeyGetEntity(instanceID, out entity))
				return true;

			return false;
		}
	}
}