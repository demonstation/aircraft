﻿using Scellecs.Morpeh;
using ServiceLib.Delegate;
using UnityEngine;

namespace Extension
{
	public static class EntityExtension
	{
		public static Entity AddComponent<T>(this Entity e, ActionRef<T> with)
			where T : struct, IComponent
		{
			if (with == null)
			{
				Debug.LogError("Delegate is null.");
				return e;
			}

			ref var component = ref e.AddComponent<T>();
			with.Invoke(ref component);
			return e;
		}

		public static Entity AddComponentAnd<T>(this Entity entity)
			where T : struct, IComponent
		{
			entity.AddComponent<T>();
			return entity;
		}
		
		public static Entity AddComponentAnd<T>(this Entity e, ActionRef<T> with)
			where T : struct, IComponent
		{
			if (with == null)
			{
				Debug.LogError("Delegate is null.");
				return e;
			}

			ref var component = ref e.AddComponent<T>();
			with.Invoke(ref component);
			return e;
		}

		public static Entity RemoveComponentAnd<T>(this Entity entity)
			where T : struct, IComponent
		{
			entity.RemoveComponent<T>();
			return entity;
		}
	}
}