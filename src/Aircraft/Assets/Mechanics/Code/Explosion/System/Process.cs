using Extension.UnityEngine;
using Scellecs.Morpeh;
using Service;
using ServiceLib.ECS.System;
using UnityEngine;
using Zenject;
using Mechanics.Explosion.Component;
using Mechanics.Health;
using Mechanics.UnityComponents;

namespace Mechanics.Explosion.System
{
	public sealed class Process : Update
	{
		const int c_overlapsArrayLenght = 5;

		[Inject] IGameFactory _factory;
		[Inject] IGameSettings _gameSettings;
		[Inject] RequestCast _healthRequest;

		Collider[] _overlaps = new Collider[c_overlapsArrayLenght];

		public override void OnAwake()
		{
			_filter = World.Filter
				.With<Exploding>()
				.With<ExplosionRequest>()
				.With<UnityComponentReference<Transform>>()
				.Build();
		}

		public override void OnUpdate(float deltaTime)
		{
			foreach (var e in _filter)
			{
				ref var exploding = ref e.GetComponent<Exploding>();
				var transform = e.GetUnityComponent<Transform>();

				var num = Physics.OverlapSphereNonAlloc(transform.position,
					exploding.ExplosionRadius, _overlaps);
				if (num == 0) continue;

				// If have some into an explosion radius
				//	then cast a damage request.
				var damage = exploding.Damage;
				for (int i = 0; i < num; i++)
				{
					var collider = _overlaps[i];
					var go = collider.attachedRigidbody.gameObject;
					if (go.TryGetEntity(out var entity) == false)
						continue;

					if (entity.Has<Health.Component.Health>() == false)
						continue;

					_healthRequest.CastDamageRequest(entity, damage);
				}

				// Hide bomb.
				var explosionPos = transform.position;
				_factory.Release(transform.gameObject);

				// Spawn an explosion effect.
				_factory.CreateBombExplosionEffect(explosionPos,
					Quaternion.Euler(_gameSettings.ExplosionEffectDirection));
			}
		}
	}
}