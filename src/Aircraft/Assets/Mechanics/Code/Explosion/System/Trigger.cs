﻿using Extension.UnityEngine;
using Mechanics.CollisionEvent.Component;
using Mechanics.Explosion.Component;
using Scellecs.Morpeh;
using ServiceLib.ECS.System;

namespace Mechanics.Explosion.System
{
	public sealed class Trigger : Update
	{
		public override void OnAwake()
		{
			_filter = World.Filter
				.With<Triggered>()
				.With<TriggerEnterEvent>()
				.Build();
		}

		public override void OnUpdate(float deltaTime)
		{
			foreach (var e in _filter)
			{
				ref var @event = ref e.GetComponent<Triggered>();
				if (@event.Sender.TryGetEntity(out var explodingEntity) == false
				    || explodingEntity.Has<Exploding>() == false
				    || explodingEntity.Has<ExplosionRequest>())
					continue;

				CastExplosionRequest(explodingEntity);
			}
		}

		void CastExplosionRequest(Entity explodingEntity)
		{
			explodingEntity.AddComponent<ExplosionRequest>();
		}
	}
}