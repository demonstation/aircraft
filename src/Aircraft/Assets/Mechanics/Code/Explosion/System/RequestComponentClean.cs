using System;
using Mechanics.Explosion.Component;
using ServiceLib.ECS.System;
using ServiceLib.ECS.System.Clean.Update;

namespace Mechanics.Explosion.System
{
	public sealed class RequestComponent : Components
	{
		protected override Type[] OneFrameComponentTypes()
		{
			return new[] { typeof(ExplosionRequest), };
		}
	}
}