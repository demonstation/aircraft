﻿using System;
using Scellecs.Morpeh;
using ServiceLib.ECS.Provider;

namespace Mechanics.Explosion.Component
{
	[Serializable]
	public struct Exploding : IComponent
	{
		public int Damage;
		public float ExplosionRadius;
	}

	public sealed class ExplodingProvider : Base<Exploding>
	{ }
}