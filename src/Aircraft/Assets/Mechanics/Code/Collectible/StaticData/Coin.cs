﻿using Mechanics.Collectible.Constant;
using UnityEngine;

namespace Mechanics.Collectible.StaticData
{
	[CreateAssetMenu(menuName = CreateAssetMenu.c_Collectible + nameof(Coin))]
	public sealed class Coin : Collectible
	{
		public const int c_Nominal = 1;
	}
}