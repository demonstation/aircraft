﻿using Scellecs.Morpeh;

namespace Mechanics.Collectible.Component
{
	public struct CollectEvent : IComponent
	{
		public Entity CollectorEntity;
		public Entity CollectibleEntity;
	}
}