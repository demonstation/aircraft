﻿using Scellecs.Morpeh;
using ServiceLib.ECS.Provider;

namespace Mechanics.Collectible.Component
{
	public struct Collector : IComponent
	{ }

	public sealed class CollectorProvider : Base<Collector>
	{ }
}