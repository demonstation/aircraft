﻿using System;
using Scellecs.Morpeh;
using ServiceLib.ECS.Provider;

namespace Mechanics.Collectible.Component
{
	[Serializable]
	public struct Collectible : IComponent
	{
		public StaticData.Collectible Data;
	}

	public sealed class CollectibleProvider : Base<Collectible>
	{ }
}