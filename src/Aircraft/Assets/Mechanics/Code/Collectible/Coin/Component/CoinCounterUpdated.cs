﻿using Scellecs.Morpeh;

namespace Mechanics.Collectible.Coin.Component
{
	public struct CoinCounterUpdated : IComponent
	{
		public Entity CoinCounterEntity;
	}
}