﻿using Scellecs.Morpeh;

namespace Mechanics.Collectible.Coin.Component
{
	public struct CoinCounterIncrease : IComponent
	{
		public int Value;
	}
}