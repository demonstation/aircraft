﻿using Scellecs.Morpeh;
using ServiceLib.ECS.Provider;

namespace Mechanics.Collectible.Coin.Component
{
	public struct CoinCounter : IComponent
	{
		public int Value;
	}

	public sealed class CoinCounterProvider : Base<CoinCounter>
	{ }
}