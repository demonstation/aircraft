﻿using Scellecs.Morpeh;
using ServiceLib.ECS.Provider;
using UnityEngine;

namespace Mechanics.Collectible.Coin.Component
{
	public struct CoinCollector : IComponent
	{
		[HideInInspector] public Entity CounterEntity;
	}

	public sealed class CoinCollectorProvider : Base<CoinCollector>
	{ }
}