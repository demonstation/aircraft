﻿using Scellecs.Morpeh;

namespace Mechanics.Collectible.Coin.Component
{
	public struct CoinCounterDecrease : IComponent
	{
		public int Value;
	}
}