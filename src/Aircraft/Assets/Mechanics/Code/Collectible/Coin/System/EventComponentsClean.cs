﻿using System;
using Mechanics.Collectible.Coin.Component;
using ServiceLib.ECS.System;
using ServiceLib.ECS.System.Clean.Update;

namespace Mechanics.Collectible.Coin.System
{
	public sealed class EventComponentsClean : Components
	{
		protected override Type[] OneFrameComponentTypes()
		{
			return new[] { typeof(CoinCollectedEvent) };
		}
	}
}