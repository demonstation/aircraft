﻿using Extension;
using Mechanics.Collectible.Coin.Component;
using Scellecs.Morpeh;
using ServiceLib.ECS.System;
using Mechanics.Collectible.Component;

namespace Mechanics.Collectible.Coin.System
{
	public sealed class ProcessCoinCounter : Update
	{
		public override void OnAwake()
		{
			_filter = World.Filter
				.With<CollectEvent>()
				.With<CoinCollectedEvent>()
				.Build();
		}

		public override void OnUpdate(float deltaTime)
		{
			foreach (var e in _filter)
			{
				var increaseValue = StaticData.Coin.c_Nominal;
				ref var coinCounter = ref CoinCounter(e, out var coinCounterEntity);
				coinCounter.Value += increaseValue;

				CastEvent(increaseValue, coinCounterEntity);
			}
		}

		void CastEvent(int increaseValue, Entity coinCounterEntity)
		{
			World.CreateEntity()
				.AddComponentAnd((ref CoinCounterUpdated c) =>
				{
					c.CoinCounterEntity = coinCounterEntity;
				})
				.AddComponent((ref CoinCounterIncrease c) =>
				{
					c.Value = increaseValue;
				});
		}

		static ref CoinCounter CoinCounter(Entity eventEntity,
			out Entity coinCounterEntity)
		{
			ref var collectEvent = ref eventEntity.GetComponent<CollectEvent>();
			ref var coinCollector =
				ref collectEvent.CollectorEntity.GetComponent<CoinCollector>();
			coinCounterEntity = coinCollector.CounterEntity;
			ref var coinCounter = ref coinCounterEntity.GetComponent<CoinCounter>();
			return ref coinCounter;
		}
	}
}