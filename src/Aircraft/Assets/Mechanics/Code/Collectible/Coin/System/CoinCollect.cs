﻿using Mechanics.Collectible.Coin.Component;
using Mechanics.Collectible.Component;
using Scellecs.Morpeh;
using Service;
using ServiceLib;
using ServiceLib.ECS.System;
using Zenject;

namespace Mechanics.Collectible.Coin.System
{
	public sealed class CoinCollect : Update
	{
		[Inject] Mechanics.Collectible.StaticData.Coin _coinData;

		public override void OnAwake()
		{
			_filter = World.Filter
				.With<CollectEvent>()
				.Build();
		}

		public override void OnUpdate(float deltaTime)
		{
			foreach (var e in _filter)
			{
				ref var @event = ref e.GetComponent<CollectEvent>();

				if (CollectibleIsCoin(in @event) == false)
					continue;

				if (HasCoinCollector(in @event) == false)
					continue;

				e.AddComponent<CoinCollectedEvent>();
			}
		}

		bool CollectibleIsCoin(in CollectEvent @event)
		{
			ref var collectible = ref @event.CollectibleEntity
				.GetComponent<Mechanics.Collectible.Component.Collectible>();

			return collectible.Data.Equals(_coinData);
		}

		bool HasCoinCollector(in CollectEvent @event)
		{
			if (@event.CollectorEntity.Has<CoinCollector>() == false)
				return false;

			ref var coinCollector =
				ref @event.CollectorEntity.GetComponent<CoinCollector>();

			if (coinCollector.CounterEntity.IsNullOrDisposed())
			{
				ErrorMessage.EntityIsNullOrDisposed();
				return false;
			}

			return true;
		}
	}
}