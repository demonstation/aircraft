﻿using Mechanics.Collectible.Coin.Component;
using Scellecs.Morpeh;
using ServiceLib.ECS.System;
using ServiceLib.ECS.System.Clean.Update;

namespace Mechanics.Collectible.Coin.System
{
	public sealed class EventEntities : Entities
	{
		protected override Filter[] CreateFilters()
		{
			return new[]
			{
				World.Filter.With<CoinCounterUpdated>().Build(),
			};
		}
	}
}