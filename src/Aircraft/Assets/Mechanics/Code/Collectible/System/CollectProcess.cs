﻿using Extension.UnityEngine;
using Mechanics.Collectible.Component;
using Mechanics.CollisionEvent.Component;
using Scellecs.Morpeh;
using ServiceLib.ECS.System;
using Zenject;

namespace Mechanics.Collectible.System
{
	public sealed class CollectProcess : Update
	{
		[Inject] EventCast _eventCast;
		
		public override void OnAwake()
		{
			_filter = World.Filter
				.With<Triggered>()
				.With<TriggerEnterEvent>()
				.Build();
		}

		public override void OnUpdate(float deltaTime)
		{
			foreach (var e in _filter)
			{
				ref var triggered = ref e.GetComponent<Triggered>();

				if (!TryGetCollector(in triggered, out var collectorEntity))
					continue;

				if (!TryGetCollectible(in triggered, out var collectibleEntity))
					continue;

				_eventCast.CastCollectEvent(collectorEntity, collectibleEntity);
			}
		}

		bool TryGetCollector(in Triggered triggered,
			out Entity collectorEntity)
		{
			var other = triggered.Other.attachedRigidbody.gameObject;
			if (other.TryGetEntity(out collectorEntity) == false)
				return false;

			if (collectorEntity.Has<Collector>() == false)
				return false;

			return true;
		}

		bool TryGetCollectible(in Triggered triggered, out Entity collectibleEntity)
		{
			var other = triggered.Sender;
			if (other.TryGetEntity(out collectibleEntity) == false)
				return false;

			if (collectibleEntity.Has<Component.Collectible>() == false)
				return false;

			return true;
		}
	}
}