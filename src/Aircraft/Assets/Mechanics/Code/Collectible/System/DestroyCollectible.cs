﻿using Mechanics.Collectible.Component;
using Mechanics.ECSBridge.Component;
using Scellecs.Morpeh;
using Service;
using ServiceLib;
using ServiceLib.ECS.System;
using Zenject;

namespace Mechanics.Collectible.System
{
	public sealed class DestroyCollectible : Update
	{
		[Inject] IGameFactory _factory;

		public override void OnAwake()
		{
			_filter = World.Filter
				.With<CollectEvent>()
				.Build();
		}

		public override void OnUpdate(float deltaTime)
		{
			foreach (var e in _filter)
			{
				ref var @event = ref e.GetComponent<CollectEvent>();
				if (CanProcessEvent(in @event) == false)
					continue;

				if (@event.CollectibleEntity.Has<EcsBridge>() == false)
				{
					ErrorMessage.EcsBridgeMissing();
					continue;
				}

				ref var ecsBridge =
					ref @event.CollectibleEntity.GetComponent<EcsBridge>();

				_factory.Release(ecsBridge.GameObject);
			}
		}

		bool CanProcessEvent(in CollectEvent @event)
		{
			if (@event.CollectorEntity.IsNullOrDisposed())
			{
				ErrorMessage.EntityIsNullOrDisposed();
				return false;
			}

			if (@event.CollectibleEntity.IsNullOrDisposed())
			{
				ErrorMessage.EntityIsNullOrDisposed();
				return false;
			}

			if (@event.CollectibleEntity.Has<Component.Collectible>() == false)
				return false;

			return true;
		}
	}
}