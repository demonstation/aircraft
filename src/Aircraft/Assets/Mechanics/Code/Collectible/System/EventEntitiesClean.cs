﻿using Mechanics.Collectible.Component;
using Scellecs.Morpeh;
using EntitiesClean = ServiceLib.ECS.System.Clean.Update.Entities;

namespace Mechanics.Collectible.System
{
	public class EventEntitiesClean : EntitiesClean
	{
		protected override Filter[] CreateFilters()
		{
			return new[]
			{
				World.Filter.With<CollectEvent>().Build(),
			};
		}
	}
}