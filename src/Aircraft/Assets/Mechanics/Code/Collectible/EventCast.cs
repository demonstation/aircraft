﻿using Extension;
using Mechanics.Collectible.Component;
using Scellecs.Morpeh;
using ServiceLib.ECS;

namespace Mechanics.Collectible
{
	public sealed class EventCast : Caster
	{
		internal void CastCollectEvent(Entity collectorEntity,
			Entity collectibleEntity)
		{
			World.CreateEntity().AddComponent((ref CollectEvent c) =>
			{
				c.CollectorEntity = collectorEntity;
				c.CollectibleEntity = collectibleEntity;
			});
		}
	}
}