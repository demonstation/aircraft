﻿using Extension;
using Mechanics.Health.Component;
using Scellecs.Morpeh;
using ServiceLib;
using ServiceLib.ECS;

namespace Mechanics.Health
{
	public sealed class RequestCast : Caster
	{
		public void CastDamageRequest(Entity healthEntity, int damageValue)
		{
			if (CanCastDamageRequest(healthEntity) == false)
				return;

			World.CreateEntity()
				.AddComponentAnd((ref HealthUpdateRequest c) =>
				{
					c.HealthEntity = healthEntity;
				})
				.AddComponent((ref Damage c) => { c.Value = damageValue; });
		}

		bool CanCastDamageRequest(Entity healthEntity)
		{
			if (healthEntity.IsNullOrDisposed())
			{
				ErrorMessage.EntityIsNullOrDisposed();
				return false;
			}

			if (healthEntity.Has<Component.Health>() == false)
			{
				ErrorMessage.EntityHasNoComponent<Component.Health>();
				return false;
			}

			return true;
		}
	}
}