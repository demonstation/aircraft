﻿using Extension;
using Mechanics.Health.Component;
using Scellecs.Morpeh;
using ServiceLib;
using ServiceLib.ECS;

namespace Mechanics.Health
{
	public sealed class EventCast : Caster
	{
		internal void CastDamageEvent(in Damage damage, Entity healthEntity)
		{
			if (CanCastDamageEvent(healthEntity) == false)
				return;

			var damageValue = damage.Value;
			World.CreateEntity()
				.AddComponentAnd((ref HealthUpdated c) =>
				{
					c.HealthEntity = healthEntity;
				})
				.AddComponent((ref Damage c) => { c.Value = damageValue; });
		}

		bool CanCastDamageEvent(Entity healthEntity)
		{
			if (healthEntity.IsNullOrDisposed())
			{
				ErrorMessage.EntityIsNullOrDisposed();
				return false;
			}
			
			if (healthEntity.Has<Component.Health>() == false)
			{
				ErrorMessage.EntityHasNoComponent<Component.Health>();
				return false;
			}

			return true;
		}
	}
}