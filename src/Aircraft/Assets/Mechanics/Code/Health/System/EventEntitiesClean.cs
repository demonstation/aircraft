﻿using Mechanics.Health.Component;
using Scellecs.Morpeh;
using ServiceLib.ECS.System.Clean.Update;

namespace Mechanics.Health.System
{
	public sealed class EventEntities : Entities
	{
		protected override Filter[] CreateFilters()
		{
			return new[]
			{
				World.Filter.With<HealthUpdated>().Build(),
			};
		}
	}
}