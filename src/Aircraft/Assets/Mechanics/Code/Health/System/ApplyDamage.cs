﻿using Extension;
using Mechanics.Health.Component;
using Scellecs.Morpeh;
using ServiceLib.ECS.System;
using Zenject;

namespace Mechanics.Health.System
{
	public class ApplyDamage : Update
	{
		[Inject] EventCast _eventCast;
		
		public override void OnAwake()
		{
			_filter = World.Filter
				.With<HealthUpdateRequest>()
				.With<Damage>()
				.Build();
		}

		public override void OnUpdate(float deltaTime)
		{
			foreach (var e in _filter)
			{
				ref var request = ref e.GetComponent<HealthUpdateRequest>();
				ref var damage = ref e.GetComponent<Damage>();
				ref var health =
					ref request.HealthEntity.GetComponent<Component.Health>();
				health.Value -= damage.Value;

				_eventCast.CastDamageEvent(in damage, request.HealthEntity);
			}
		}
	}
}