﻿using Scellecs.Morpeh;

namespace Mechanics.Health.Component
{
	public struct HealthUpdated : IComponent
	{
		public Entity HealthEntity;
	}
}