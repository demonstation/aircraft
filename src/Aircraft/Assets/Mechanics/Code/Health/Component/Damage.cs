﻿using Scellecs.Morpeh;

namespace Mechanics.Health.Component
{
	public struct Damage : IComponent
	{
		public int Value;
	}
}