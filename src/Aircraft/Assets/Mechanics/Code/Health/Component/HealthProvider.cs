﻿using System;
using Scellecs.Morpeh;
using ServiceLib.ECS.Provider;

namespace Mechanics.Health.Component
{
	[Serializable]
	public struct Health : IComponent
	{
		public int Max;
		public int Min;
		public int Value;
	}

	public sealed class HealthProvider : Base<Health>
	{ }
}