﻿using Scellecs.Morpeh;

namespace Mechanics.Health.Component
{
	internal struct HealthUpdateRequest : IComponent
	{
		public Entity HealthEntity;
	}
}