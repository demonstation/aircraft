using System;
using Scellecs.Morpeh;
using ServiceLib.ECS.Provider;
using UnityEngine;

namespace Mechanics.ECSBridge.Component
{
	[Serializable]
	public struct OwnerReference : IEcsBridgeComponent
	{
		public Entity Entity { get; set; }
		[field: SerializeField] public GameObject GameObject { get; private set; }
	}

	public sealed class OwnerReferenceProvider : Base<OwnerReference>
	{ }
}