﻿using Extension.UnityEngine;
using Mechanics.ECSBridge.Component;
using Scellecs.Morpeh;
using UnityEngine;
using Zenject;

namespace Mechanics.ECSBridge.System
{
	public sealed class EcsBridgeInit : ISystem
	{
		[Inject] public World World { get; set; }

		Filter _ecsBridgeFilter;
		Filter _rootGameObjectReferenceFilter;

		public void OnAwake()
		{
			_ecsBridgeFilter = World.Filter
				.With<EcsBridge>()
				.Without<Initialized>()
				.Build();

			_rootGameObjectReferenceFilter = World.Filter
				.With<OwnerReference>()
				.Without<Initialized>()
				.Build();
		}


		public void OnUpdate(float deltaTime)
		{
			Init<EcsBridge>(_ecsBridgeFilter);
			Init<OwnerReference>(_rootGameObjectReferenceFilter);
		}

		void Init<T>(Filter filter) where T : struct, IEcsBridgeComponent
		{
			foreach (var e in filter)
			{
				ref var bridge = ref e.GetComponent<T>();
				if (bridge.GameObject.TryGetEntity(out var bridgeEntity) == false)
				{
					Debug.LogError(
						$"{bridge.GameObject.name} game object has not entity.");
					continue;
				}

				bridge.Entity = bridgeEntity;
				e.AddComponent<Initialized>();
			}
		}

		public void Dispose()
		{ }
	}
}