﻿using Scellecs.Morpeh;
using UnityEngine;

namespace Mechanics.ECSBridge
{
	public interface IEcsBridgeComponent : IComponent
	{
		public Entity Entity { get; set; }
		public GameObject GameObject { get; }
	}
}