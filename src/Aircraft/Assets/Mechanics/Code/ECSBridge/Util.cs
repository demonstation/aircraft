﻿using Extension.UnityEngine;
using Mechanics.ECSBridge.Component;
using Scellecs.Morpeh;
using UnityEngine;

namespace Mechanics.ECSBridge
{
	public static class Util
	{
		public static bool TryGetOwner(GameObject gameObject, out Entity entity)
		{
			if (gameObject.TryGetEntity(out entity) == false ||
			    entity.Has<OwnerReference>() == false)
				return false;

			return true;
		}
	}
}