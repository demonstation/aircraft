﻿using Extension.UnityEngine;
using Mechanics.CameraManeuvering.Component;
using Mechanics.UnityComponents;
using Scellecs.Morpeh;
using ServiceLib.ECS.System;
using UnityEngine;

namespace Mechanics.CameraManeuvering.System
{
	public sealed class RotationTracking : LateUpdate
	{
		public override void OnAwake()
		{
			_filter = World.Filter
				.With<RotationTracker>()
				.With<UnityComponentReference<Transform>>()
				.Build();
		}

		public override void OnUpdate(float deltaTime)
		{
			foreach (var e in _filter)
			{
				ref var tracker = ref e.GetComponent<RotationTracker>();
				if (tracker.Target == null)
					continue;

				ref var transformRef =
					ref e.GetComponent<UnityComponentReference<Transform>>();
				var transform = transformRef.Ref;

				var direction = transform.VectorTo(tracker.Target);
				var lookRotation = Quaternion.LookRotation(direction);
				transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation,
					tracker.Ratio * deltaTime);
			}
		}
	}
}