﻿using Mechanics.CameraManeuvering.Component;
using Mechanics.UnityComponents;
using Scellecs.Morpeh;
using ServiceLib.ECS.System;
using UnityEngine;

namespace Mechanics.CameraManeuvering.System
{
	public sealed class Following : LateUpdate
	{
		public override void OnAwake()
		{
			_filter = World.Filter
				.With<CameraInterpolationFollow>()
				.With<UnityComponentReference<Transform>>()
				.Build();
		}

		public override void OnUpdate(float deltaTime)
		{
			foreach (var e in _filter)
			{
				ref var interpolation = ref e.GetComponent<CameraInterpolationFollow>();

				ref var transformRef =
					ref e.GetComponent<UnityComponentReference<Transform>>();

				if (interpolation.Target == null || transformRef.Ref == null)
					continue;

				var transform = transformRef.Ref;

				transform.position = Vector3.SmoothDamp(transform.position,
					interpolation.Target.position, ref interpolation.CurrentVelocity,
					interpolation.SmoothTime);
			}
		}
	}
}