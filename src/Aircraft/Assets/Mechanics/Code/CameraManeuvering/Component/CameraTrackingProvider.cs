﻿using Scellecs.Morpeh;
using ServiceLib.ECS.Provider;
using UnityEngine;

namespace Mechanics.CameraManeuvering.Component
{
	public struct CameraTracking : IComponent
	{
		public Transform Target;
	}

	public sealed class CameraTrackingProvider : Base<CameraTracking>
	{ }
}