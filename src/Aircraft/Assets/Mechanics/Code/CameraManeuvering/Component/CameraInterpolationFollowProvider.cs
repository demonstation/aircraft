﻿using System;
using Scellecs.Morpeh;
using ServiceLib.ECS.Provider;
using UnityEngine;

namespace Mechanics.CameraManeuvering.Component
{
	[Serializable]
	public struct CameraInterpolationFollow : IComponent
	{
		public float SmoothTime;
		public Transform Target;
		public Vector3 CurrentVelocity;
	}

	public sealed class CameraInterpolationFollowProvider : Base<CameraInterpolationFollow>
	{ }
}