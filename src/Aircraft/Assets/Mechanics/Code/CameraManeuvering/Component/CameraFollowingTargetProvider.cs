﻿using System;
using Scellecs.Morpeh;
using ServiceLib.ECS.Provider;
using UnityEngine;

namespace Mechanics.CameraManeuvering.Component
{
	[Serializable]
	public struct CameraFollowingTarget : IComponent
	{
		public Transform Value;
	}

	public sealed class
		CameraFollowingTargetProvider : Base<CameraFollowingTarget>
	{ }
}