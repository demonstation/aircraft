﻿using System;
using Scellecs.Morpeh;
using ServiceLib.ECS.Provider;
using UnityEngine;

namespace Mechanics.CameraManeuvering.Component
{
	[Serializable]
	public struct RotationTracker : IComponent
	{
		public float Ratio;
		public Transform Target;
	}

	public sealed class RotationTrackerProvider : Base<RotationTracker>
	{ }
}