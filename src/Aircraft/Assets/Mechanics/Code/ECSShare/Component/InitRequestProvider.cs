﻿using Scellecs.Morpeh;
using ServiceLib.ECS.Provider;

namespace Mechanics.ECSShare.Component
{
	public struct InitRequest : IComponent
	{ }

	public sealed class InitRequestProvider : Base<InitRequest>
	{ }
}