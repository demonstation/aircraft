﻿using System;
using Scellecs.Morpeh;
using ServiceLib.ECS.Provider;
using UnityEngine;

namespace Mechanics.ECSShare.Component
{
	[Serializable]
	public struct DestroyTimer : IComponent
	{
		public float Value;
		public GameObject Target;
	}

	public sealed class DestroyTimerProvider : Base<DestroyTimer>
	{ }
}