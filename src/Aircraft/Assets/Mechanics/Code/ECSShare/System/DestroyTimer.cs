﻿using Scellecs.Morpeh;
using ServiceLib.ECS.System;
using UnityEngine;

namespace Mechanics.ECSShare.System
{
	public sealed class DestroyTimer : Update
	{
		public override void OnAwake()
		{
			_filter = World.Filter
				.With<Mechanics.ECSShare.Component.DestroyTimer>()
				.Build();
		}

		public override void OnUpdate(float deltaTime)
		{
			foreach (var e in _filter)
			{
				ref var destroyTimer =
					ref e.GetComponent<Mechanics.ECSShare.Component.DestroyTimer>();
				Object.Destroy(destroyTimer.Target, destroyTimer.Value);
				e.RemoveComponent<Mechanics.ECSShare.Component.DestroyTimer>();
			}
		}
	}
}