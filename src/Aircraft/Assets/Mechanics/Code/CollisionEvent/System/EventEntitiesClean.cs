﻿using Mechanics.CollisionEvent.Component;
using Scellecs.Morpeh;
using ServiceLib.ECS;
using ServiceLib.ECS.System.Clean;
using Zenject;

namespace Mechanics.CollisionEvent.System
{
	public sealed class EventEntitiesClean : ICleanupSystem
	{
		EntityCleaner _entityCleaner;

		[Inject] public World World { get; set; }

		public void OnAwake()
		{
			_entityCleaner = new EntityCleaner(World, CreateFilters());
		}

		public void OnUpdate(float deltaTime)
		{
			_entityCleaner.OnUpdate();
		}

		Filter[] CreateFilters()
		{
			return new[]
			{
				World.Filter.With<TriggerEnterEvent>().Build(),
				World.Filter.With<TriggerExitEvent>().Build(),
				World.Filter.With<CollisionEnterEvent>().Build(),
				World.Filter.With<CollisionExitEvent>().Build(),
			};
		}

		public void Dispose()
		{ }
	}
}