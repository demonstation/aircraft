using Mechanics.CollisionEvent.Component;
using Scellecs.Morpeh;
using UnityEngine;
using Collision = Mechanics.CollisionEvent.Component.Collision;

namespace Mechanics.CollisionEvent
{
	public static class Util
	{
		public static void CastEvent<TEvent>(World world, GameObject thisGameObject,
			UnityEngine.Collision collision)
			where TEvent : struct, IColliderEventComponent
		{
			var e = world.CreateEntity();
			e.AddComponent<TEvent>();
			ref var collisionInfo = ref e.AddComponent<Collision>();
			collisionInfo.Other = collision.gameObject;
			collisionInfo.Sender = thisGameObject;
		}

		public static void CastEvent<TEvent>(World world, GameObject thisGameObject,
			Collider other)
			where TEvent : struct, IColliderEventComponent
		{
			var e = world.CreateEntity();
			e.AddComponent<TEvent>();
			ref var triggerInfo = ref e.AddComponent<Triggered>();
			triggerInfo.Other = other;
			triggerInfo.Sender = thisGameObject;
		}
	}
}