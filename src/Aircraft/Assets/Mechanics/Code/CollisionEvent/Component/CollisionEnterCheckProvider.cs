﻿namespace Mechanics.CollisionEvent.Component
{
	public struct CollisionEnterCheck : IColliderCheckComponent
	{ }

	public struct CollisionEnterEvent : IColliderEventComponent
	{ }

	public sealed class
		CollisionEnterCheckProvider : ColliderCheckProvider<CollisionEnterCheck>
	{
		void OnCollisionEnter(UnityEngine.Collision collision)
		{
			Util.CastEvent<CollisionEnterEvent>(_world, gameObject, collision);
		}
	}
}