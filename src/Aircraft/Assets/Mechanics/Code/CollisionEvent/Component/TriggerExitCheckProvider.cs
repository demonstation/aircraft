﻿using UnityEngine;

namespace Mechanics.CollisionEvent.Component
{
	public struct TriggerExitCheck : IColliderCheckComponent
	{ }

	public struct TriggerExitEvent : IColliderEventComponent
	{ }

	public sealed class
		TriggerExitCheckProvider : ColliderCheckProvider<TriggerExitCheck>
	{
		void OnTriggerExit(Collider other)
		{
			Util.CastEvent<TriggerExitEvent>(_world, gameObject, other);
		}
	}
}