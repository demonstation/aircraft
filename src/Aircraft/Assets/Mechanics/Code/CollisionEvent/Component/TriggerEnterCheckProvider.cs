﻿using UnityEngine;

namespace Mechanics.CollisionEvent.Component
{
	public struct TriggerEnterCheck : IColliderCheckComponent
	{ }

	public struct TriggerEnterEvent : IColliderEventComponent
	{ }

	public sealed class
		TriggerEnterCheckProvider : ColliderCheckProvider<TriggerEnterCheck>
	{
		void OnTriggerEnter(Collider other)
		{
			Util.CastEvent<TriggerEnterEvent>(_world, gameObject, other);
		}
	}
}