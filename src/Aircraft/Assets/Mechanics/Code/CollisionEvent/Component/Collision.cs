using Scellecs.Morpeh;
using UnityEngine;

namespace Mechanics.CollisionEvent.Component
{
	public struct Collision : IComponent
	{
		public GameObject Other;
		public GameObject Sender;
	}
}