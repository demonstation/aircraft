﻿namespace Mechanics.CollisionEvent.Component
{
	public struct CollisionExitCheck : IColliderCheckComponent
	{ }

	public struct CollisionExitEvent : IColliderEventComponent
	{ }

	public sealed class
		CollisionExitCheckProvider : ColliderCheckProvider<CollisionExitCheck>
	{
		void OnCollisionExit(UnityEngine.Collision collision)
		{
			Util.CastEvent<CollisionExitEvent>(_world, gameObject, collision);
		}
	}
}