﻿using Scellecs.Morpeh;
using ServiceLib.ECS.Provider;
using UnityEngine;
using Zenject;

namespace Mechanics.CollisionEvent.Component
{
	public abstract class ColliderCheckProvider<TComponent> : Base<TComponent>
		where TComponent : struct, IColliderCheckComponent
	{
		[Inject] protected World _world;
	}
}