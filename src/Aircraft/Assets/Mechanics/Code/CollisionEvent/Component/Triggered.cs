﻿using Scellecs.Morpeh;
using UnityEngine;

namespace Mechanics.CollisionEvent.Component
{
	public struct Triggered : IComponent
	{
		public Collider Other;
		public GameObject Sender;
	}
}