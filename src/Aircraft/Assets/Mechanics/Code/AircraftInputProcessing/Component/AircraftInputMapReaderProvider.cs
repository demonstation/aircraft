﻿using Scellecs.Morpeh;
using ServiceLib.ECS.Provider;

namespace Mechanics.AircraftInputProcessing.Component
{
	public struct AircraftInputMapReader : IComponent
	{ }

	public sealed class
		AircraftInputMapReaderProvider : Base<AircraftInputMapReader>
	{ }
}