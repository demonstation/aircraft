﻿using Mechanics.Shooting.Component;
using Scellecs.Morpeh;

namespace Mechanics.AircraftInputProcessing.System
{
	public static class Util
	{
		public static void CastShootRequest(Filter filter)
		{
			foreach (var e in filter)
				e.AddComponent<ShootRequest>();
		}
	}
}