﻿using Mechanics.AircraftInputProcessing.Component;
using Mechanics.Shooting.Component;
using Scellecs.Morpeh;
using Service;
using Service.Input;
using ServiceLib.ECS.System;

namespace Mechanics.AircraftInputProcessing.System
{
	public sealed class AutomaticShootingProcessing : Update
	{
		readonly IAircraft _input;
		readonly World _world;

		bool _shootIsPressed;
		Filter _requestFilter;

		public AutomaticShootingProcessing(IInput input, World world)
		{
			_world = world;
			_input = input.Map<IAircraft>();
		}

		public override void OnAwake()
		{
			_input.ShotStarted += OnAircraftShotStarted;
			_input.ShotCanceled += OnAircraftShotCanceled;

			_filter = _world.Filter
				.With<Shooter>()
				.With<AutomaticShooter>()
				.With<AircraftInputMapReader>()
				.Without<ShootRequest>()
				.Build();
		}

		public override void OnUpdate(float deltaTime)
		{
			if (_shootIsPressed == false) return;
			Util.CastShootRequest(_filter);
		}

		public override void Dispose()
		{
			_input.ShotStarted -= OnAircraftShotStarted;
			_input.ShotCanceled -= OnAircraftShotCanceled;
		}

		void OnAircraftShotStarted() =>
			_shootIsPressed = true;

		void OnAircraftShotCanceled() =>
			_shootIsPressed = false;
	}
}