﻿using Mechanics.AircraftInputProcessing.Component;
using Mechanics.Maneuvering.Component;
using Scellecs.Morpeh;
using Service;
using Service.Input;
using ServiceLib.ECS.System;
using UnityEngine;
using Zenject;

namespace Mechanics.AircraftInputProcessing.System
{
	public sealed class TorqueProcessing : Update
	{
		IAircraft _inputMap;

		[Inject]
		void Construct(IInput inputService)
		{
			_inputMap = inputService.Map<IAircraft>();
		}

		public override void OnAwake()
		{
			_filter = World.Filter
				.With<AddTorqueRotation>()
				.With<AircraftInputMapReader>()
				.Build();
		}

		public override void OnUpdate(float deltaTime)
		{
			foreach (var e in _filter)
			{
				ref var addTorque = ref e.GetComponent<AddTorqueRotation>();
				var torque = addTorque.Value;
				addTorque.Value =
					new Vector3(_inputMap.AircraftManeuvering, torque.y, torque.z);
			}
		}
	}
}