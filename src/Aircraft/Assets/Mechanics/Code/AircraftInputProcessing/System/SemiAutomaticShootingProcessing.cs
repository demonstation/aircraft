﻿using Mechanics.AircraftInputProcessing.Component;
using Mechanics.Shooting.Component;
using Scellecs.Morpeh;
using Service;
using Service.Input;
using ServiceLib.ECS.System;

namespace Mechanics.AircraftInputProcessing.System
{
	public sealed class SemiAutomaticShootingProcessing : Initializer
	{
		readonly IAircraft _input;
		readonly World _world;

		public SemiAutomaticShootingProcessing(IInput input, World world)
		{
			_world = world;
			_input = input.Map<IAircraft>();
		}

		public override void OnAwake()
		{
			_input.ShotPerformed += OnAircraftShotPerformed;

			_filter = _world.Filter
				.With<Shooter>()
				.With<SemiAutomaticShooter>()
				.With<AircraftInputMapReader>()
				.Without<ShootRequest>()
				.Build();
		}

		public override void Dispose()
		{
			_input.ShotPerformed -= OnAircraftShotPerformed;
		}

		void OnAircraftShotPerformed()
		{
			Util.CastShootRequest(_filter);
		}
	}
}