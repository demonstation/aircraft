﻿using Service;
using UnityEditor;
using UnityEngine;
using Zenject;

namespace Mechanics
{
	public class DevHelper : MonoBehaviour
	{
		[Inject] ISceneLoader _sceneLoader;

		[MenuItem("🛠/Launch this scene.")]
		static void LaunchThisScene()
		{
			var helper = FindObjectOfType<DevHelper>();
			var name = UnityEngine.SceneManagement.SceneManager.GetActiveScene().name;
			helper._sceneLoader.LoadAsync(name);
		}
	}
}