﻿using System;
using Scellecs.Morpeh;
using ServiceLib.ECS.Provider;
using UnityEngine;

namespace Mechanics.Shooting.Component
{
	[Serializable]
	public struct Shooter : IComponent
	{
		public float ProjectileSpeed;
		public Transform ProjectileSocket;
	}

	public sealed class ShooterProvider : Base<Shooter>
	{ }
}