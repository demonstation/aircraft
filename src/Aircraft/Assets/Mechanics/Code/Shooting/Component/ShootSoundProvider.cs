﻿using System;
using Scellecs.Morpeh;
using ServiceLib.ECS.Provider;
using UnityEngine;

namespace Mechanics.Shooting.Component
{
	[Serializable]
	public struct ShootSound : IComponent
	{
		public AudioSource AudioSource;
	}

	public sealed class ShootSoundProvider : Base<ShootSound> { }
}