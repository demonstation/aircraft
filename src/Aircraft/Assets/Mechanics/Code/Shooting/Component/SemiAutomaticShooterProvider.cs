﻿using Scellecs.Morpeh;
using ServiceLib.ECS.Provider;

namespace Mechanics.Shooting.Component
{
	public struct SemiAutomaticShooter : IComponent
	{ }

	public sealed class
		SemiAutomaticShooterProvider : Base<SemiAutomaticShooter>
	{ }
}