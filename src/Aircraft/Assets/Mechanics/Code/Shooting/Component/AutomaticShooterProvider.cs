﻿using System;
using Scellecs.Morpeh;
using ServiceLib.ECS.Provider;
using UnityEngine;

namespace Mechanics.Shooting.Component
{
	[Serializable]
	public struct AutomaticShooter : IComponent
	{
		public float ShotsFrequency;
		[HideInInspector]
		public float PreviousShotTimeMoment;
	}

	public sealed class AutomaticShooterProvider : Base<AutomaticShooter> { }
}