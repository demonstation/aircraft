﻿using Mechanics.Shooting.Component;
using Scellecs.Morpeh;
using Service;
using ServiceLib;
using ServiceLib.ECS.System;
using UnityEngine;
using Zenject;

namespace Mechanics.Shooting.System
{
	public sealed class ShootMaker : Update
	{
		[Inject] IGameFactory _factory;

		public override void OnAwake()
		{
			_filter = World.Filter
				.With<Shooter>()
				.With<ShootCommand>()
				.Build();
		}

		public override void OnUpdate(float deltaTime)
		{
			foreach (var e in _filter)
			{
				ref var shooter = ref e.GetComponent<Shooter>();

				if (CheckShooterData(shooter.ProjectileSocket) == false)
					continue;

				var projectileInstance = _factory.BulletProjectile(
					shooter.ProjectileSocket.position, shooter.ProjectileSocket.rotation);
				SetVelocity(projectileInstance, ref shooter);

				e.AddComponent<ShootEvent>();

				e.RemoveComponent<ShootCommand>();
			}
		}

		void SetVelocity(GameObject projectileInstance, ref Shooter shooter)
		{
			var rigidbody = projectileInstance.GetComponent<Rigidbody>();
			rigidbody.velocity =
				shooter.ProjectileSocket.forward * shooter.ProjectileSpeed;
		}

		bool CheckShooterData(Transform projectileSocket)
		{
			if (projectileSocket == null)
			{
				ErrorMessage.ComponentMissingData<Shooter>();
				return false;
			}

			return true;
		}
	}
}