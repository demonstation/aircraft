﻿using Mechanics.Shooting.Component;
using Scellecs.Morpeh;
using ServiceLib.ECS.System;

namespace Mechanics.Shooting.System
{
	public sealed class ShootSound : Update
	{
		public override void OnAwake()
		{
			_filter = World.Filter
				.With<ShootEvent>()
				.With<Shooter>()
				.With<Component.ShootSound>()
				.Build();
		}

		public override void OnUpdate(float deltaTime)
		{
			foreach (var e in _filter)
			{
				ref var shootSound =
					ref e.GetComponent<Component.ShootSound>();
				var audioSource = shootSound.AudioSource;
				audioSource.Play();
			}
		}
	}
}