﻿using Mechanics.Shooting.Component;
using Scellecs.Morpeh;
using ServiceLib.ECS.System;

namespace Mechanics.Shooting.System
{
	public sealed class SemiAutomaticShooting : Update
	{
		public override void OnAwake()
		{
			_filter = World.Filter
				.With<ShootRequest>()
				.With<Shooter>()
				.With<SemiAutomaticShooter>()
				.Build();
		}

		public override void OnUpdate(float deltaTime)
		{
			foreach (var e in _filter)
			{
				e.AddComponent<ShootCommand>();
			}
		}
	}
}