﻿using Mechanics.Shooting.Component;
using Scellecs.Morpeh;
using ServiceLib.ECS.System;
using UnityEngine;

namespace Mechanics.Shooting.System
{
	public sealed class AutomaticShooting : Update
	{
		public override void OnAwake()
		{
			_filter = World.Filter
				.With<ShootRequest>()
				.With<Shooter>()
				.With<AutomaticShooter>()
				.Build();
		}

		public override void OnUpdate(float deltaTime)
		{
			foreach (var e in _filter)
			{
				ref var automaticShooter = ref e.GetComponent<AutomaticShooter>();
				if (CanShoot(automaticShooter.PreviousShotTimeMoment,
					    automaticShooter.ShotsFrequency) == false)
					continue;

				e.AddComponent<ShootCommand>();

				SavePreviousShotTimeMoment(ref automaticShooter);
			}
		}

		void SavePreviousShotTimeMoment(ref AutomaticShooter shooter) => 
			shooter.PreviousShotTimeMoment = Time.time;

		bool CanShoot(float previousShotTimeMoment, float shotsFrequency)
		{
			var delta = Time.time - previousShotTimeMoment;
			var period = 1 / shotsFrequency;
			if (delta < period)
				return false;
			return true;
		}
	}
}