﻿using System;
using Mechanics.Shooting.Component;
using ServiceLib.ECS.System;
using ServiceLib.ECS.System.Clean.Update;

namespace Mechanics.Shooting.System
{
	public sealed class EventComponents : Components
	{
		protected override Type[] OneFrameComponentTypes()
		{
			return new[] { typeof(ShootEvent), };
		}
	}
}