using UnityEngine;

namespace Mechanics.UnityComponents
{
	public sealed class RigidBodyProvider : UnityComponentProvider<Rigidbody>
	{ }
}