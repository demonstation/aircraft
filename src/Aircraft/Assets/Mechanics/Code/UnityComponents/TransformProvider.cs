﻿using UnityEngine;

namespace Mechanics.UnityComponents
{
	public sealed class TransformProvider : UnityComponentProvider<Transform>
	{ }
}