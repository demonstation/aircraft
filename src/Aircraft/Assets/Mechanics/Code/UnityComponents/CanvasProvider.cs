using UnityEngine;

namespace Mechanics.UnityComponents
{
	public sealed class CanvasProvider : UnityComponentProvider<Canvas>
	{ }
}