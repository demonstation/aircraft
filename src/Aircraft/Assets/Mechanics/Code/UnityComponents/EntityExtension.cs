using Scellecs.Morpeh;

namespace Mechanics.UnityComponents
{
	public static class EntityExtension
	{
		public static ref UnityComponentReference<T> GetUnityComponentRef<T>(
			this Entity entity)
			where T : UnityEngine.Component
		{
			return ref entity.GetComponent<UnityComponentReference<T>>();
		}

		public static T GetUnityComponent<T>(this Entity entity)
			where T : UnityEngine.Component
		{
			ref var refComponent =
				ref entity.GetComponent<UnityComponentReference<T>>();
			return refComponent.Ref;
		}
	}
}