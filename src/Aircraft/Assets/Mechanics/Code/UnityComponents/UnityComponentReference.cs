﻿using System;
using Scellecs.Morpeh;
using ServiceLib.ECS.Provider;

namespace Mechanics.UnityComponents
{
	[Serializable]
	public struct UnityComponentReference<TComponent> : IComponent
		where TComponent : UnityEngine.Component
	{
		public TComponent Ref;
	}

	public abstract class UnityComponentProvider<TComponent> :
		Base<UnityComponentReference<TComponent>>
		where TComponent : UnityEngine.Component
	{
		protected override void Initialize()
		{
			ref var component = ref GetData();
			if (component.Ref == null)
				component.Ref = GetComponent<TComponent>();
		}
	}
}