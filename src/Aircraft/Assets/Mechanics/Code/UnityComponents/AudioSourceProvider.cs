﻿using UnityEngine;

namespace Mechanics.UnityComponents
{
	public sealed class AudioSourceProvider : UnityComponentProvider<AudioSource>
	{ }
}