using Mechanics.UnityComponents;
using Scellecs.Morpeh;
using ServiceLib.ECS.System;
using UnityEngine;

namespace Mechanics.Maneuvering.System
{
	public sealed class AddForceMovement : FixedUpdate
	{
		public override void OnAwake()
		{
			_filter = World.Filter
				.With<Component.AddForceMovement>()
				.With<UnityComponentReference<Rigidbody>>()
				.Build();
		}

		public override void OnUpdate(float deltaTime)
		{
			foreach (var e in _filter)
			{
				ref var addForce = ref e.GetComponent<Component.AddForceMovement>();
				ref var rigidBodyRef =
					ref e.GetComponent<UnityComponentReference<Rigidbody>>();

				var rb = rigidBodyRef.Ref;
				var transform = rb.transform;
				var force = transform.forward * addForce.Direction.z +
					transform.up * addForce.Direction.y +
					transform.right * addForce.Direction.x;
				rb.AddForce(force, addForce.Mode);
			}
		}
	}
}