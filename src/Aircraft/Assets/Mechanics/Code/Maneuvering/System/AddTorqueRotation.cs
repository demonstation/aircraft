﻿using Mechanics.UnityComponents;
using Scellecs.Morpeh;
using ServiceLib.ECS.System;
using Extension.UnityEngine;
using UnityEngine;

namespace Mechanics.Maneuvering.System
{
	public sealed class AddTorqueRotation : FixedUpdate
	{
		public override void OnAwake()
		{
			_filter = World.Filter
				.With<Component.AddTorqueRotation>()
				.With<UnityComponentReference<Rigidbody>>()
				.Build();
		}

		public override void OnUpdate(float deltaTime)
		{
			foreach (var e in _filter)
			{
				ref var addTorque = ref e.GetComponent<Component.AddTorqueRotation>();
				ref var rigidbodyRef =
					ref e.GetComponent<UnityComponentReference<Rigidbody>>();
				var rb = rigidbodyRef.Ref;
				var transform = rb.transform;
				var torque =
					transform.Left() * addTorque.Value.x +
					transform.up * addTorque.Value.y +
					transform.forward * addTorque.Value.z;

				rb.AddTorque(torque, addTorque.Mode);
			}
		}
	}
}