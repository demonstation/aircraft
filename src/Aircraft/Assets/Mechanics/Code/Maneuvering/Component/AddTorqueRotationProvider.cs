﻿using System;
using Scellecs.Morpeh;
using ServiceLib.ECS.Provider;
using UnityEngine;

namespace Mechanics.Maneuvering.Component
{
	[Serializable]
	public struct AddTorqueRotation : IComponent
	{
		public Vector3 Value;
		public ForceMode Mode;
	}

	public sealed class AddTorqueRotationProvider : Base<AddTorqueRotation> { }
}