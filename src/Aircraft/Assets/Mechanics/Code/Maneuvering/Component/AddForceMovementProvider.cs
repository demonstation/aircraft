using System;
using Scellecs.Morpeh;
using ServiceLib.ECS.Provider;
using UnityEngine;

namespace Mechanics.Maneuvering.Component
{
	[Serializable]
	public struct AddForceMovement : IComponent
	{
		public Vector3 Direction;
		public ForceMode Mode;
	}

	public sealed class AddForceMovementProvider : Base<AddForceMovement>
	{ }
}