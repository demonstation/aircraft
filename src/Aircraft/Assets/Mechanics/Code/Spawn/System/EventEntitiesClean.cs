﻿using Mechanics.Spawn.Component;
using Scellecs.Morpeh;
using ServiceLib.ECS.System;
using ServiceLib.ECS.System.Clean.Update;

namespace Mechanics.Spawn.System
{
	public sealed class EventEntities : Entities
	{
		protected override Filter[] CreateFilters()
		{
			return new[] { World.Filter.With<SpawnedEvent>().Build(), };
		}
	}
}