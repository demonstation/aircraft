﻿using Extension;
using Extension.UnityEngine;
using Mechanics.Spawn.Component;
using Mechanics.UnityComponents;
using Scellecs.Morpeh;
using Service;
using ServiceLib;
using ServiceLib.ECS.System;
using UnityEngine;
using Zenject;

namespace Mechanics.Spawn.System
{
	public sealed class Aircraft : Update
	{
		[Inject] IGameFactory _factory;

		public override void OnAwake()
		{
			_filter = World.Filter
				.With<SpawnPoint>()
				.With<SpawnRequest>()
				.With<Component.Aircraft>()
				.With<UnityComponentReference<Transform>>()
				.Build();
		}

		public override void OnUpdate(float deltaTime)
		{
			foreach (var e in _filter)
			{
				ref var transformRef =
					ref e.GetComponent<UnityComponentReference<Transform>>();

				var (aircraftObj, aircraftEntity) =
					CreateAircraft(transformRef.Ref, _factory);

				CastCreatedEvent(aircraftEntity);
			}
		}

		public static (GameObject obj, Entity entity)
			CreateAircraft(Transform spawnPoint, IGameFactory factory)
		{
			var spawnTransform = spawnPoint;
			var aircraftObj = factory.CreateAircraft(spawnTransform.position,
				spawnTransform.rotation);

			if (aircraftObj.TryGetEntity(out var aircraftEntity) == false)
				ErrorMessage.HasNoEntityForGameObject(aircraftObj.name);

			return (aircraftObj, aircraftEntity);
		}

		void CastCreatedEvent(Entity aircraftEntity)
		{
			World.CreateEntity()
				.AddComponentAnd((ref SpawnedEvent c) => c.Spawned = aircraftEntity)
				.AddComponent<Component.Aircraft>();
		}
	}
}