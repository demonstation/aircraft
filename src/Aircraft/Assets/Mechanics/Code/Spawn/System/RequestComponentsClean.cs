﻿using System;
using Mechanics.Spawn.Component;
using CleanComponents = ServiceLib.ECS.System.Clean.Update.Components;

namespace Mechanics.Spawn.System
{
	public sealed class RequestComponentsClean : CleanComponents
	{
		protected override Type[] OneFrameComponentTypes()
		{
			return new[] { typeof(SpawnRequest), };
		}
	}
}