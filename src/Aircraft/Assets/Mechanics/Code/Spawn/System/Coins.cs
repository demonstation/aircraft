﻿using Mechanics.Spawn.Component;
using Mechanics.Spawn.CoordinatesCalculator;
using Scellecs.Morpeh;
using Service;
using ServiceLib.ECS.System;
using UnityEngine;
using Zenject;

namespace Mechanics.Spawn.System
{
	public sealed class Coins : Update
	{
		[Inject] IGameFactory _factory;
		ICoordinatesCalculator _coordinatesCalculator;

		public override void OnAwake()
		{
			_coordinatesCalculator = new Integer(5, 5);
			
			_filter = World.Filter
				.With<Coin>()
				.With<SpawnArea>()
				.With<SpawnRequest>()
				.Build();
		}

		public override void OnUpdate(float deltaTime)
		{
			foreach (var e in _filter)
			{
				var spawnArea = e.GetComponent<SpawnArea>();
				var points = _coordinatesCalculator.Calculate(
					spawnArea.FirstAngel.position, spawnArea.SecondAngel.position, 0);

				foreach (var point in points)
					_factory.CreateCoin(point, Quaternion.identity, spawnArea.Parent);
			}
		}
	}
}