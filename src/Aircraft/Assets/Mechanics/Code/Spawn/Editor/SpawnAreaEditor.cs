using Mechanics.Spawn.Component;
using UnityEditor;
using UnityEngine;

namespace Mechanics.Spawn.Editor
{
	// [CustomEditor(typeof(SpawnAreaProvider))]
	public sealed class SpawnAreaEditor : UnityEditor.Editor
	{
		[DrawGizmo(GizmoType.NonSelected | GizmoType.Selected)]
		public static void DrawArea(SpawnAreaProvider provider, GizmoType gizmoType)
		{
			ref var spawnArea = ref provider.GetData();
			if (!spawnArea.FirstAngel || !spawnArea.SecondAngel)
				return;

			var firstPoint = spawnArea.FirstAngel.position;
			var secondPoint = spawnArea.SecondAngel.position;
			var delta = secondPoint - firstPoint;

			DrawVerticalLines(firstPoint, delta, secondPoint);
			DrawHorizontalLines(firstPoint, delta, secondPoint);
		}

		static void DrawVerticalLines(Vector3 firstPoint, Vector3 delta,
			Vector3 secondPoint)
		{
			var end = new Vector3(firstPoint.x, firstPoint.y + delta.y, firstPoint.z);
			Gizmos.DrawLine(firstPoint, end);

			end = new Vector3(secondPoint.x, secondPoint.y - delta.y, secondPoint.z);
			Gizmos.DrawLine(secondPoint, end);
		}

		static void DrawHorizontalLines(Vector3 firstPoint, Vector3 delta,
			Vector3 secondPoint)
		{
			var end = new Vector3(firstPoint.x + delta.x, firstPoint.y, firstPoint.z);
			Gizmos.DrawLine(firstPoint, end);

			end = new Vector3(secondPoint.x - delta.x, secondPoint.y, secondPoint.z);
			Gizmos.DrawLine(secondPoint, end);
		}
	}
}