﻿using Scellecs.Morpeh;

namespace Mechanics.Spawn.Component
{
	public struct SpawnedEvent : IComponent
	{
		public Entity Spawned;
	}
}