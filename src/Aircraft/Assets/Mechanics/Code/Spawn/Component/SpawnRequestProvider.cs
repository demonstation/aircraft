﻿using Scellecs.Morpeh;
using ServiceLib.ECS.Provider;

namespace Mechanics.Spawn.Component
{
	public struct SpawnRequest : IComponent
	{ }

	public sealed class SpawnRequestProvider : Base<SpawnRequest>
	{ }
}