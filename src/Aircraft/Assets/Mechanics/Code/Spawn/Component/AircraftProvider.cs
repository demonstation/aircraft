﻿using Scellecs.Morpeh;
using ServiceLib.ECS.Provider;

namespace Mechanics.Spawn.Component
{
	public struct Aircraft : IComponent
	{ }

	public sealed class AircraftProvider : Base<Aircraft>
	{ }
}