﻿using System;
using Scellecs.Morpeh;
using ServiceLib.ECS.Provider;
using UnityEngine;

namespace Mechanics.Spawn.Component
{
	[Serializable]
	public struct SpawnArea : IComponent
	{
		public Transform Parent;
		public Transform FirstAngel;
		public Transform SecondAngel;
	}

	public sealed class SpawnAreaProvider : Base<SpawnArea>
	{ }
}