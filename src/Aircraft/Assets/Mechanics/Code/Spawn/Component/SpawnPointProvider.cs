﻿using Scellecs.Morpeh;
using ServiceLib.ECS.Provider;

namespace Mechanics.Spawn.Component
{
	public struct SpawnPoint : IComponent
	{ }

	public sealed class SpawnPointProvider : Base<SpawnPoint>
	{ }
}