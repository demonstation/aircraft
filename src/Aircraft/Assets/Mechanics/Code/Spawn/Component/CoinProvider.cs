﻿using Scellecs.Morpeh;
using ServiceLib.ECS.Provider;

namespace Mechanics.Spawn.Component
{
	public struct Coin : IComponent
	{ }

	public sealed class CoinProvider : Base<Coin>
	{ }
}