﻿using System.Collections.Generic;
using UnityEngine;

namespace Mechanics.Spawn.CoordinatesCalculator
{
	public interface ICoordinatesCalculator
	{
		IReadOnlyList<Vector3> Calculate(Vector3 firstPoint, Vector3 secondPoint,
			float z);
	}
}