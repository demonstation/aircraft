﻿using System.Collections.Generic;
using UnityEngine;

namespace Mechanics.Spawn.CoordinatesCalculator
{
	public sealed class Integer : ICoordinatesCalculator
	{
		float _deltaX = 1;
		float _deltaY = 1;

		public Integer(float deltaX, float deltaY)
		{
			_deltaX = deltaX;
			_deltaY = deltaY;
		}

		public IReadOnlyList<Vector3> Calculate(
			Vector3 firstPoint, Vector3 secondPoint, float z)
		{
			float xRatio = secondPoint.x > firstPoint.x ? 1 : -1f;
			float yRatio = secondPoint.y > firstPoint.y ? 1 : -1;

			List<float> xCoordinates = new();
			List<float> yCoordinates = new();

			CreateCoordinates(firstPoint.x, xRatio, secondPoint.x, _deltaX,
				xCoordinates);
			CreateCoordinates(firstPoint.y, yRatio, secondPoint.y, _deltaY,
				yCoordinates);

			List<Vector3> result = new();
			for (int i = 0; i < xCoordinates.Count; i++)
			{
				for (int j = 0; j < yCoordinates.Count; j++)
				{
					var vector3 = new Vector3(xCoordinates[i], yCoordinates[j], z);
					result.Add(vector3);
				}
			}

			return result;
		}

		void CreateCoordinates(float value, float ratio, float endPoint,
			float delta, List<float> coordinates)
		{
			if (ratio > 0)
			{
				while (value < endPoint)
					AddNextCoordinate(ref value, ratio, delta, coordinates);
			}
			else
			{
				while (value > endPoint)
					AddNextCoordinate(ref value, ratio, delta, coordinates);
			}
		}

		void AddNextCoordinate(ref float value, float ratio, float delta,
			List<float> coordinates)
		{
			value += delta * ratio;
			coordinates.Add(value);
		}
	}
}