﻿using Scellecs.Morpeh;
using ServiceLib.ECS.System.Clean.CleanUp;

namespace Mechanics.HUD.Health.System
{
	public sealed class RequestEntityClean : Entities
	{
		protected override Filter[] CreateFilters()
		{
			return new[]
			{
				World.Filter.With<Component.UpdateScreenRequest>().Build(),
			};
		}
	}
}