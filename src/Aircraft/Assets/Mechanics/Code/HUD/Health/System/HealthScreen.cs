﻿using Scellecs.Morpeh;
using ServiceLib.ECS.System;

namespace Mechanics.HUD.Health.System
{
	public sealed class HealthScreen : LateUpdate
	{
		public override void OnAwake()
		{
			_filter = World.Filter
				.With<Component.UpdateScreenRequest>()
				.Build();
		}

		public override void OnUpdate(float deltaTime)
		{
			foreach (var e in _filter)
			{
				var request = e.GetComponent<Component.UpdateScreenRequest>();
				
				ref var screen =
					ref request.ScreenEntity.GetComponent<Component.HealthScreen>();

				ref var health = ref request.HealthEntity
					.GetComponent<Mechanics.Health.Component.Health>();

				screen.Text.text = health.Value.ToString();
			}
		}
	}
}