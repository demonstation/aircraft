﻿using Extension;
using Mechanics.Health.Component;
using Mechanics.HUD.Health.Component;
using Scellecs.Morpeh;
using ServiceLib;
using ServiceLib.ECS.System;

namespace Mechanics.HUD.Health.System
{
	public sealed class UpdateScreenRequest : Update
	{
		public override void OnAwake()
		{
			_filter = World.Filter
				.With<HealthUpdated>()
				.Build();
		}

		public override void OnUpdate(float deltaTime)
		{
			foreach (var e in _filter)
			{
				var @event = e.GetComponent<HealthUpdated>();
				var healthEntity = @event.HealthEntity;

				if (HasTracker(healthEntity) == false)
					continue;
				ref var healthTracker = ref healthEntity.GetComponent<HealthTracker>();
				var healthScreenEntity = healthTracker.HealthScreenEntity;

				if (CheckComponentData(healthScreenEntity, healthEntity) == false)
					continue;

				CastUpdateScreenRequest(healthEntity, healthScreenEntity);
			}
		}

		static bool CheckComponentData(Entity healthScreenEntity,
			Entity healthEntity)
		{
			if (healthScreenEntity.Has<Component.HealthScreen>() == false)
			{
				ErrorMessage.EntityHasNoComponent<Component.HealthScreen>();
				return false;
			}

			ref var healthScreen =
				ref healthScreenEntity.GetComponent<Component.HealthScreen>();

			if (healthScreen.TrackerEntity.ID.Equals(healthEntity.ID) == false)
			{
				ErrorMessage.NotMatchEntityIds();
				return false;
			}

			return true;
		}

		void CastUpdateScreenRequest(Entity healthEntity, Entity healthScreenEntity)
		{
			World.CreateEntity()
				.AddComponent((ref Component.UpdateScreenRequest c) =>
				{
					c.HealthEntity = healthEntity;
					c.ScreenEntity = healthScreenEntity;
				});
		}

		bool HasTracker(Entity healthEntity)
		{
			return healthEntity.Has<HealthTracker>();
		}
	}
}