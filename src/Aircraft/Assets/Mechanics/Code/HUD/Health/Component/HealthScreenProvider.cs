﻿using System;
using Scellecs.Morpeh;
using ServiceLib.ECS.Provider;
using TMPro;
using UnityEngine;

namespace Mechanics.HUD.Health.Component
{
	[Serializable]
	public struct HealthScreen : IComponent
	{
		public TMP_Text Text;
		[HideInInspector] public Entity TrackerEntity;
	}

	public class HealthScreenProvider : Base<HealthScreen>
	{ }
}