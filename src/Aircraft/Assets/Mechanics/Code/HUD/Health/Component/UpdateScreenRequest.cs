﻿using Scellecs.Morpeh;

namespace Mechanics.HUD.Health.Component
{
	public struct UpdateScreenRequest : IComponent
	{
		public Entity ScreenEntity;
		public Entity HealthEntity;
	}
}