﻿using System;
using Scellecs.Morpeh;
using ServiceLib.ECS.Provider;
using TMPro;
using UnityEngine;

namespace Mechanics.HUD.Coin.Component
{
	[Serializable]
	public struct CoinScreen : IComponent
	{
		public TMP_Text Text;
		[HideInInspector] public Entity TrackerEntity;
	}

	public sealed class CoinScreenProvider : Base<CoinScreen>
	{ }
}