﻿using Mechanics.Collectible.Coin.Component;
using Mechanics.HUD.Coin.Component;
using Scellecs.Morpeh;
using ServiceLib;
using ServiceLib.ECS.System;

namespace Mechanics.HUD.Coin.System
{
	public sealed class CoinScreen : LateUpdate
	{
		public override void OnAwake()
		{
			_filter = World.Filter
				.With<CoinCounterUpdated>()
				.Build();
		}

		public override void OnUpdate(float deltaTime)
		{
			foreach (var e in _filter)
			{
				ref var coinCounterUpdate = ref e.GetComponent<CoinCounterUpdated>();

				if (HasCoinTracker(in coinCounterUpdate) == false)
					continue;
				var coinTracker = CoinTracker(in coinCounterUpdate);

				if (CanGetCoinScreen(in coinTracker) == false)
					continue;
				ref var coinScreen = ref CoinScreenComponent(coinTracker);

				UpdateText(in coinCounterUpdate, in coinScreen);
			}
		}

		bool HasCoinTracker(in CoinCounterUpdated coinCounterUpdated)
		{
			return coinCounterUpdated.CoinCounterEntity.Has<CoinTracker>();
		}

		ref CoinTracker CoinTracker(in CoinCounterUpdated coinCounterUpdated)
		{
			return ref coinCounterUpdated.CoinCounterEntity
				.GetComponent<Component.CoinTracker>();
		}

		bool CanGetCoinScreen(in CoinTracker coinTracker)
		{
			if (coinTracker.CoinScreenEntity.IsNullOrDisposed())
			{
				ErrorMessage.EntityIsNullOrDisposed();
				return false;
			}

			if (coinTracker.CoinScreenEntity.Has<Component.CoinScreen>() == false)
			{
				ErrorMessage.EntityHasNoComponent<Component.CoinScreen>();
				return false;
			}

			return true;
		}

		ref Component.CoinScreen CoinScreenComponent(CoinTracker coinTracker)
		{
			return ref coinTracker.CoinScreenEntity
				.GetComponent<Component.CoinScreen>();
		}

		void UpdateText(in CoinCounterUpdated coinCounterUpdated,
			in Component.CoinScreen coinScreen)
		{
			ref var coinCounter = ref coinCounterUpdated.CoinCounterEntity
				.GetComponent<Collectible.Coin.Component.CoinCounter>();

			coinScreen.Text.text = coinCounter.Value.ToString();
		}
	}
}